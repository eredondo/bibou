# Short term (november 2020)
- tests interface avancé, recherche de bugs/opérations manquantes
    - erreurs PySimpleGUI:
        - duplicated key due to multiple usages of im_top2 in many classes (warning)
        - warning change_look_and_feel('Dark Blue 3')
        - problèmes avec les nouvelles versions de PySimpleGUI (>4.11.0)
- écrire instructions installation
    - apt install python3-tk pip3 
    - apt install python3-venv si on veut faire un environnement
    - pip(3) install -r requirements.txt
    - run desktop file creator
- bibou:
    - self.sql.lend_book(id_book,id_user): change this function to take LOAN_DURATION from settings file
- new book, catalog:
    - allow multiple authors as in new_book_isbn (author_search.py)
- update script:
    - backup settings file + database
    - git pull
    - restore settings file + database
- Use logging
	- on catalog modifications
	- on user list modifications
	- on sql operations (DEBUG)
# High priority

## Tests/scripts:
- command line tools / external_data_tools?
    - database rm tables (ne laisser que le catalogue)
    - import a db file of books
- make a demo database (demo.db), e.g. 10 users, 20 books
- write a python script to generate some loans on database (demo, history, delayed returns...)
- write a python script to update dates of loans (testing of delayed returns, for instance)
- functional analysis (bibou)
    - make a list of each function (lend, return, etc.)
    - make a test for each function (write a procedure?)
- functional analysis (bibou_advanced)
    - make a list of each function (create book/user, modify book/user, import from file, export to file, etc.)
    - make a test for each function (write a procedure?)
## Ergonomy:
- bibou/new loan:
    - buttons with arrows for add book to cart, remove and empty cart
- bibou/new return:
    - buttons with arrows for add book to cart, remove and empty cart
## Code quality, error detection
- check/remove print messages
- write __init__.py for bibou_modules:
    - import bibou_modules = from submodule import Class
    - logging.basicconfig and config reading on __init__.py?

# Long term
- check if max loan when loading user to put it in red
- statistics:
    - users by number of loans (Table)
    - history: list of loans of a user (Table)
    - books by loan number (Table)
    - history: list of loans of a book (Table)
    - authors by loan number (Table)
- selected element in list + ENTER = 'OK'
- tools
    - script to create bat file (as bibou_installer for desktop files)
- SQL: Character encoding (sorting accents, etc.)
- Interface simple
    - Prêts en cours :
        - surlignage des livres en retard
        - tri par ancienneté ou par élève
- Graphics
    - bleu du fond moins flash
    - couleurs de l'hibou plus flash
    - boutons: texte en gras
