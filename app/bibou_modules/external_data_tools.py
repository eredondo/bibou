#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Tools to create a new database, import data from external tools (e.g. user list or book list),
    and to export data
"""
# TODO check error treatment in all functions
# TODO check log file messages in all  functions
# TODO id_other_authors >>> new id

import re
import sqlite3
import xlrd

if __package__ is None or __package__ == "":
    from common import *
else:
    from .common import *

logging.basicConfig(filename="bibou.log", level=logging.INFO,
                    format='%(asctime)s %(message)s',
                    datefmt='%d/%m/%Y %H:%M:%S')

def create_new_database(in_sql_file, out_db_file):
    '''
        Create a new database from a SQL script file containing all tables and
        variables definitions (USER, BOOK, EDITOR, AUTHOR, LOAN, etc.)
    '''
    if not os.path.isfile(in_sql_file):
        logging.error("ERROR:create_new_database:SQL script file not found")
        return
    if len(out_db_file) == 0:
        logging.error("ERROR:create_new_database: empty output database filename")
        return
    if os.path.isfile(out_db_file):
        logging.error("ERROR:create_new_database: output database file already exists")
        return
    con = sqlite3.connect(out_db_file)
    with open(in_sql_file, 'r') as sql_file:
        sql_script = sql_file.read()
    cursor = con.cursor()
    try:
        cursor.executescript(sql_script)
    except sqlite3.Error as e:
        logging.error("ERROR:create_new_database: could not create output database (" + e.args[0] + ")")
        #throw error to stop calling function
    con.commit()
    con.close()
    logging.info("INFO:create_new_database: output database file created at: " + out_db_file)

def test_xls_user_file(xls_file):
    '''
        Function to check a xls file (user list).
        First line is intended to be for column titles.
        The xls file must contain three or four columns.
        If there are three columns: FIRSTNAME, LASTNAME, LEVEL.
        If there are four columns: ID,FIRSTNAME, LASTNAME, LEVEL.
        A three columns xls file is for function import_users while a four
        columns xls file if for function update_users.
    '''
    #TODO more tests on data consistency (cell type text/number depending of column)
    if not os.path.isfile(xls_file):
        logging.error("ERROR:test_xls_user_file: user file not found")
        return
    xls_book = xlrd.open_workbook(xls_file)
    xls_sheet = xls_book.sheet_by_index(0)
    first_row = xls_sheet.row_values(0)
    return len(first_row)

def import_users(xls_file, out_db_file):
    '''
        Function to import a user list from a xls file.
        The xls file must contain three columns: FIRSTNAME, LASTNAME, LEVEL.
        First line is intended to be for column titles.
    '''
    if not os.path.isfile(xls_file):
        logging.error("ERROR:import_users: user list file not found")
        return
    if not os.path.isfile(out_db_file):
        logging.error("ERROR:import_users: output database file not found")
        return
    xls_book = xlrd.open_workbook(xls_file)
    xls_sheet = xls_book.sheet_by_index(0)
    r = []
    for i in range(1, xls_sheet.nrows):
        r.append(xls_sheet.row_values(i))
    #r = [p[0:2] + [int(p[2])] for p in r]
    r = [tuple(p[0:2] + [int(p[2])]) for p in r]
    con_out = sqlite3.connect(out_db_file)
    result_1 = con_out.execute("SELECT FIRSTNAME, LASTNAME, LEVEL FROM USER").fetchall()
    for p in r:
        if p not in result_1:
            logging.info("import_users: INSERT USER "
                          + ", ".join(p[0:2])
                          + ", " + str(p[2]))
            p1 = (clean_none_to_empty_text(p[0]),
                  clean_none_to_empty_text(p[1]),
                  clean_none_to_empty_text(p[2]))
            con_out.execute("INSERT INTO USER (FIRSTNAME, LASTNAME, LEVEL) VALUES ('"
                            + p1[0] + "','" + p1[1] + "',"+ str(p1[2]) + ");")
    con_out.commit()
    con_out.close()

def update_users(xls_file, out_db_file):
    '''
        Function to update a user list from a xls file.
        The xls file must contain four columns: ID, FIRSTNAME, LASTNAME, LEVEL.
        First line is intended to be for column titles.
    '''
    if not os.path.isfile(xls_file):
        logging.error("ERROR:import_users: user list file not found")
        return
    if not os.path.isfile(out_db_file):
        logging.error("ERROR:import_users: output database file not found")
        return
    xls_book = xlrd.open_workbook(xls_file)
    xls_sheet = xls_book.sheet_by_index(0)
    r = []
    for i in range(1, xls_sheet.nrows):
        r.append(xls_sheet.row_values(i))
    r = [[int(p[0])] + p[1:3] + [int(p[3])] for p in r]
    con_out = sqlite3.connect(out_db_file)
    
    for user in r:
        sql = "UPDATE USER SET FIRSTNAME = '" + user[1] \
                  + "' WHERE ID = " + str(user[0])+ ";"
        con_out.execute(sql)
        sql = "UPDATE USER SET LASTNAME = '" + user[2] \
                  + "' WHERE ID = " + str(user[0])+ ";"
        con_out.execute(sql)
        sql = "UPDATE USER SET LEVEL = " + str(user[3]) \
                  + " WHERE ID = " + str(user[0])+ ";"
        con_out.execute(sql)

    con_out.commit()
    con_out.close()

def import_editors(in_db_file, out_db_file):
    '''
        Take PUBLISHER DISTINCT names from BOOK table in in_db_file and insert these names
        in out_db_file in EDITOR table as EDITOR.NAME (existing names will be ignored)
    '''
    if not os.path.isfile(in_db_file):
        logging.error("ERROR:import_editors: input database file not found")
        return
    if not os.path.isfile(out_db_file):
        logging.error("ERROR:import_editors: output database file not found")
        return
    con_in = sqlite3.connect(in_db_file)
    con_out = sqlite3.connect(out_db_file)
    r = con_in.execute("SELECT DISTINCT PUBLISHER FROM BOOK").fetchall()
    #formatting rules
    r = [(format_editor_name(s[0]),) for s in r]
    #removing duplicates
    r = list(set(r))
    logging.info("import_authors:  found "+ str(len(r))+ " editors")
    con_in.close()
    result_1 = con_out.execute("SELECT DISTINCT NAME FROM EDITOR").fetchall()
    result_1 = [p for p in r if p not in result_1]
    if result_1:
        logging.info("import_editors:  trying to import "+ str(len(result_1))+ " new editors")
    else:
        logging.info("import_editors:  "+ str(len(result_1))+ " new editors")
    try:
        con_out.executemany("INSERT INTO EDITOR (NAME) VALUES (?)", result_1)
        con_out.commit()
    except sqlite3.Error as e:
        logging.error("ERROR:import_editors: something went wrong (" + e.args[0] + ")")
    con_out.close()

def import_authors(in_db_file, out_db_file):
    '''
        Takes FIRSTNAME and LASTNAME in AUTHOR table in in_db_file and insert these
        values into AUTHOR table in out_db_file (existing values will be ignored)
    '''
    if not os.path.isfile(in_db_file):
        logging.error("ERROR:import_authors: input database file not found")
        return
    if not os.path.isfile(out_db_file):
        logging.error("ERROR:import_authors: output database file not found")
        return
    con_in = sqlite3.connect(in_db_file)
    con_out = sqlite3.connect(out_db_file)
    #new way
    r = con_in.execute("SELECT FIRSTNAME, LASTNAME FROM AUTHOR").fetchall()
    con_in.close()
    logging.info("import_authors:  found "+ str(len(r))+ " authors")
    result_1 = con_out.execute("SELECT FIRSTNAME, LASTNAME FROM AUTHOR").fetchall()
    #FIXME allow null in out database?
    result_2 = [(format_author_name(p[0]),
                 format_author_name(p[1])) for p in r]
    result_2 = [p for p in result_2 if p not in result_1]
    if result_2:
        logging.info("import_authors: trying to import "+ str(len(result_2))+ " new authors")
    else:
        logging.info("import_authors: "+ str(len(result_2))+ " new authors")
    try:
        con_out.executemany("INSERT INTO AUTHOR (FIRSTNAME, LASTNAME) VALUES (?,?)", result_2)
        con_out.commit()
    except sqlite3.Error as e:
        logging.error("ERROR:import_authors: something went wrong (" + e.args[0] + ")")

    con_out.close()

def import_books(in_db_file, out_db_file):
    '''
        Takes info in table BOOK in in_db_file and format it to put it into BOOK table
        in out_db_file.
        It checks for author id (different in in_db_file respect to out_db_file).
        It searches for editor ID.
        It checks if REF if ok (no empty, no duplicates).
        Existing books (same REF, same TITLE) are ignored
    '''
    if not os.path.isfile(in_db_file):
        logging.error("ERROR:import_books: input database file not found")
        return
    if not os.path.isfile(out_db_file):
        logging.error("ERROR:import_books: output database file not found")
        return
    #logging.info("import_books")
    #1. take data from in_db_file
    con_in = sqlite3.connect(in_db_file)
    #cursorIn = con_in.cursor()
    sql = "SELECT ID, SERIES, TITLE, ISBN, AUTHOR, ADDITIONAL_AUTHORS, PUBLISHER,"+\
" SUMMARY, COMMENTS, COVER_PATH, CATEGORIES, PAGES, PUBLISHED_DATE FROM BOOK"
    #cursorIn.execute(sql)
    r = con_in.execute(sql).fetchall()
    #con_in.close()
    logging.info("import_books: found " + str(len(r)) + " books")

    #2. books with empty series cannot be inserted in out_db_file >> log file
    result_0 = [p for p in r if p[1] is None]
    if result_0:
        logging.error("ERROR:import_books: found " + str(len(result_0)) \
                      + " books without REF, will not be imported")
    for p in result_0:
        r_author = con_in.execute("SELECT FIRSTNAME,LASTNAME FROM AUTHOR WHERE ID=" + str(p[4])).fetchall()
        if not(r_author):
            r_author = [("","")]
#        if len(r_author)>0:
        logging.error("ERROR:import_books: ED:"
                      + format_editor_name(p[6]) + ", AUT:"
                      + format_author_name(r_author[0][0]) + ", "
                      + format_author_name(r_author[0][1]) + ", TIT:'"
                      + p[2] + "' book without REF, will not be imported")
#        else:
#            logging.error("ERROR:import_books: ED:"
#                          + format_editor_name(p[6]) + ", TIT:'"
#                          + p[2] + "' book without REF, will not be imported")
    #3. check which new_ids are already in out_db_file >> log file
    #3.1 tranform SERIES (title, volume) to out_db_file.ID
    result_1 = [p for p in r if not p[1] is None]
    series = [p[1] for p in result_1]
    new_id = [series_to_new_id(s) for s in series]
    #3.2 put new_id as first column and remove first and second columns (ID,series)
    result_1 = list(zip(new_id, result_1))
    result_1 = [(p[0],)+p[1][2:] for p in result_1]
    #3.3 books with empty REF (i.e. invalid series_to_new_id) cannot be inserted in out_db_file >> log file
    result_0 = [p for p in result_1 if p[0] is None]
    for p in result_0:
        r_author = con_in.execute("SELECT FIRSTNAME,LASTNAME FROM AUTHOR WHERE ID=" + str(p[3])).fetchall()
        if len(r_author)>0:
            logging.error("ERROR:import_books: '" + format_editor_name(p[5]) + ", "+ format_author_name(r_author[0][0]) + ", "+ format_author_name(r_author[0][1]) + ", " + p[1] + "' book without REF, will not be imported")
        else:
            logging.error("ERROR:import_books: '" + p[5] + ", " + p[1] + "' book without REF (invalid series), will not be imported")
    result_1 = [p for p in result_1 if not p[0] is None]
    #3.4 take existing id from out_db_file
    con_out = sqlite3.connect(out_db_file)
    result_3 = con_out.execute("SELECT REF,TITLE FROM BOOK").fetchall()
    refs = [p[0] for p in result_3]
    titles = [p[1] for p in result_3]
    #3.5 split result_1 into result_0 (already existing) and result_2 (new entries)
    result_0 = [p for p in result_1 if p[0] in refs and p[1] in titles]
    if result_0:
        logging.warning("import_books: " + str(len(result_0)) \
                        + " books already in database, will not be imported")
    result_2 = [p for p in result_1 if p not in result_0]
    #3.6 search author id in outDB and update
    author_old_id = [p[3] for p in result_2]
    #TODO error handling here
    r = [con_in.execute("SELECT FIRSTNAME,LASTNAME FROM AUTHOR WHERE ID="
                        + str(oid)).fetchall() for oid in author_old_id]
    #TODO r is [[(a,b)],[(c,d)],...[(y,z)]], why?
    #TODO error handling here
    r = [p[0] for p in r]
    author_new_id = [con_out.execute('SELECT DISTINCT ID FROM AUTHOR WHERE FIRSTNAME="'
                                     +format_author_name(p[0])+'" AND LASTNAME="'
                                     +format_author_name(p[1])+'"').fetchall() for p in r]
    #only first result (prevent duplications
    author_new_id = [a[0][0] for a in author_new_id]
    result_2 = list(zip(author_new_id, result_2))
    result_2 = [p[1][0:3]+(p[0],)+p[1][4:] for p in result_2]
    #3.7 change editor names by their ids
    editor_names = [format_editor_name(p[5]) for p in result_2]
    editor_ids = []
    for editor_name in editor_names:
        editor_ids.append(con_out.execute("SELECT ID FROM EDITOR WHERE NAME=?",
                                          (editor_name, )).fetchall())
    editor_ids = [e[0][0] for e in editor_ids]
    result_2 = list(zip(editor_ids, result_2))
    #p[1][0:5] = ID,TITLE,ISBN,ID_AUTHOR,ID_OTHER_AUTHORS
    #(p[0],) = ID_EDITOR
    #p[1][6] = ABSTRACT
    #"" = KEYWORDS
    #p[1][6:] = COMMENTS,COVER_PATH,CATEGORIES,PAGES,PUBLISHED_DATE
    #(0,) = STATE
    result_2 = [p[1][0:5]+(p[0],)+ (p[1][6],) + ("",) + p[1][7:]+(0,) for p in result_2]
    #3.8 format_title
    result_2 = [(p[0],) + (format_title(p[1]),) + p[2:] for p in result_2]
    #3.9 insert result_2 into out_db_file
    if result_2:
        sql = "INSERT INTO BOOK (REF,TITLE,ISBN,ID_AUTHOR,ID_OTHER_AUTHORS,ID_EDITOR,ABSTRACT,"+\
              "KEYWORDS,COMMENTS,COVER_PATH,CATEGORIES,PAGES,PUBLISHED_DATE,STATE) VALUES "+\
              "(?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        logging.info("import_books: trying to import " + str(len(result_2)) + " books")
        for p in result_2:
            try:
                con_out.execute(sql, p)
                con_out.commit()
                logging.debug("import_books: REF=" + p[0] + "TITLE='"+ p[1] + "' OK")
            except sqlite3.Error as e:
                sql_a = "SELECT FIRSTNAME,LASTNAME FROM AUTHOR WHERE ID=" + str(p[3])
                r_author = con_out.execute(sql_a).fetchall()
                sql_e = "SELECT NAME FROM EDITOR WHERE ID=" + str(p[5])
                r_editor = con_out.execute(sql_e).fetchall()
                if not(r_author):
                    r_author = [("","")]
                if not(r_editor):
                    r_editor = [("",)]
#                if len(r_author)>0:
                logging.error("ERROR:import_books: " + p[0] + ", ED:"
                              + format_editor_name(r_editor[0][0]) + ", AUT:"
                              + format_author_name(r_author[0][0]) + ", "
                              + format_author_name(r_author[0][1]) + ", TIT:'"
                              + p[1] + "' ("+e.args[0]+")")
#                else:
#                    logging.error("ERROR:import_books: " + p[0] + ", ED:"
#                                  + format_editor_name(r_editor[0]) + ", TIT:'"
#                                  + p[1] + "' ("+e.args[0]+")")
                #logging.error("ERROR:import_books: error on "+ p[0] + ", '"+ p[1] + "' ("+e.args[0]+")")
    else:
        logging.info("import_books: nothing to import")
    con_out.close()
    con_in.close()
def clean_none_to_empty_text(s):
    '''Transform a None value to an empty string ("").
        This function is used to avoid inserting None in a table where NULL is not allowed
    '''
    if s is None:
        s = ""
    return s
def series_to_new_id_old(s):
    '''
        Transform series in mylibrary.db to id in newDB
        ex.: '[{"title":"A","volume":406}]' > 'A406'
    '''
    #TODO use regex to detect good format in s
    if not isinstance(s, str):
        return None
    s = s.replace('[{"title":"', '')
    s = s.replace('","volume":', '')
    s = s.replace('}]', '')
    return s

def series_to_new_id(s):
    '''
        Transform series in mylibrary.db to id in newDB
        ex.: '[{"title":"A","volume":406}]' > 'A406'
    '''
    if not isinstance(s, str):
        return None
    s1 = re.search('(?<="title":")[A-Z]+',s)
    s2 = re.search('(?<="volume":)[0-9]+',s)
    if (s1 is None) or (s2 is None):
        return None
    s1 = s1.group(0)
    s2 = s2.group(0)
    s3 = s1 + s2.zfill(4)
    return s3

def format_editor_name(s):
    '''Some formatting rules for Editors:
        1.- Title Case
        2.- Dashes must be surrounded by one space each side (not "a-word" but "a - word")
        3.- Accents in uppercase vowels are ignored: i.e. Ë,Ê,É,È >> E
        4.- Names starting by Edition, Editions, Ed., Eds.
    '''
    if s is None:
        return ""
    s = clean_HTML(s)
    #1.- Title Case
    s = s.title()
    #2.- Dashes must be surrounded by one space each side (not "a-word" but "a - word")
    #2.1.- Split into "-" words, then trim surrounding espaces with strip()
    S = [s1.strip() for s1 in s.split('-')]
    #2.2.- join elements of S with " - "
    s = " - ".join(S)
    #3.- Accents in uppercase vowels are ignored: i.e. Ë,Ê,É,È >> E
    s = remove_uppercase_accents(s)
    #4.- Names starting by Edition, Editions, Ed., Eds.
    if s.startswith('Editions '):
        s = s.replace('Editions ','Ed. ',1)
    if s.startswith('Edition '):
        s = s.replace('Edition ','Ed. ',1)
    if s.startswith('Eds. '):
        s = s.replace('Eds. ','Ed. ',1)
    #if s.startswith('Ed. '):
    #    s = s.replace('Ed. ','',1)
    if s.startswith('Eds '):
        s = s.replace('Eds ','Ed. ',1)
    if s.startswith('Ed '):
        s = s.replace('Ed ','Ed. ',1)
    #5.- Other
    if s.endswith('.'):
        s = s[:-1]

    return s

def format_author_name(s):
    '''Some formatting rules for Authors:
        1.- Title Case
        3.- Accents in uppercase vowels are ignored: i.e. Ë,Ê,É,È >> E
    '''
    #TODO (or not): None replaced by empty strings
    #if None, error in import_books when searching new atuhor's id
    if s is None:
        return ""
    s = clean_HTML(s)
    #1.- Title Case
    s = s.title()
    #3.- Accents in uppercase vowels are ignored: i.e. Ë,Ê,É,È >> E
    s = remove_uppercase_accents(s)
    return s

def format_title(s):
    '''Some formatting rules for Book titles:
        1.- first letter upper other "as is"
        3.- Accents in uppercase vowels are ignored: i.e. Ë,Ê,É,È >> E
    '''
    if s is None:
        return s
    s = clean_HTML(s)
    #1.- First letter upper
    s = s[0].upper() + s[1:]
    #3.- Accents in uppercase vowels are ignored: i.e. Ë,Ê,É,È >> E
    s = remove_uppercase_accents(s)
    return s

def remove_uppercase_accents(s):
    '''
        Remove accents in uppercase letters: e.g. Ë,Ê,É,È >> E
    '''
    #ascii 192-197 >>> A
    for i in range(192,198):
        s = s.replace(chr(i),'A')
    #ascii 198 >>> AE
    s = s.replace(chr(198),'AE')
    #ascii 199 >>> C
    s = s.replace(chr(199),'C')
    #ascii 200-203 >>> E
    for i in range(200,204):
        s = s.replace(chr(i),'E')
    #ascii 204-207 >>> I
    for i in range(204,208):
        s = s.replace(chr(i),'I')
    s = s.replace(chr(208),'D')
    s = s.replace(chr(209),'N')
    #ascii 210-214 >>> O
    for i in range(210,215):
        s = s.replace(chr(i),'O')
    #ascii 216 >>> O
    s = s.replace(chr(216),'O')
    #ascii 217-220 >>> U
    for i in range(217,221):
        s = s.replace(chr(i),'U')
    #ascii 221 >>> Y
    s = s.replace(chr(221),'Y')
    return s

def clean_HTML(s):
    s = s.replace("&#39;","'")
    s = s.replace('&Agrave;','À')
    s = s.replace('&Aacute;','Á')
    s = s.replace('&Acirc;','Â')
    s = s.replace('&Atilde;','Ã')
    s = s.replace('&Auml;','Ä')
    s = s.replace('&Aring;','Å')
    s = s.replace('&AElig;','Æ')
    s = s.replace('&Ccedil;','Ç')
    s = s.replace('&Egrave;','È')
    s = s.replace('&Eacute;','É')
    s = s.replace('&Ecirc;','Ê')
    s = s.replace('&Euml;','Ë')
    s = s.replace('&Igrave;','Ì')
    s = s.replace('&Iacute;','Í')
    s = s.replace('&Icirc;','Î')
    s = s.replace('&Iuml;','Ï')
    s = s.replace('&ETH;','Ð')
    s = s.replace('&Ntilde;','Ñ')
    s = s.replace('&Ograve;','Ò')
    s = s.replace('&Oacute;','Ó')
    s = s.replace('&Ocirc;','Ô')
    s = s.replace('&Otilde;','Õ')
    s = s.replace('&Ouml;','Ö')
    s = s.replace('&times;','×')
    s = s.replace('&Oslash;','Ø')
    s = s.replace('&Ugrave;','Ù')
    s = s.replace('&Uacute;','Ú')
    s = s.replace('&Ucirc;','Û')
    s = s.replace('&Uuml;','Ü')
    s = s.replace('&Yacute;','Ý')
    s = s.replace('&agrave;','à')
    s = s.replace('&aacute;','á')
    s = s.replace('&acirc;','â')
    s = s.replace('&atilde;','ã')
    s = s.replace('&auml;','ä')
    s = s.replace('&aring;','å')
    s = s.replace('&aelig;','æ')
    s = s.replace('&ccedil;','ç')
    s = s.replace('&egrave;','è')
    s = s.replace('&eacute;','é')
    s = s.replace('&ecirc;','ê')
    s = s.replace('&euml;','ë')
    s = s.replace('&igrave;','ì')
    s = s.replace('&iacute;','í')
    s = s.replace('&icirc;','î')
    s = s.replace('&iuml;','ï')
    return s

def import_library(in_sql_file, in_db_file, out_db_file):
    '''
        Import everything needed in a library: create new database, import authors,
        import editors, import books.
    '''
    #0. logging
    logging.info("importLibrary: input=" + in_db_file + ", output=" + out_db_file)
    #1. create database
    create_new_database(in_sql_file, out_db_file)
    #2. import authors
    import_authors(in_db_file, out_db_file)
    #3. import editors
    import_editors(in_db_file, out_db_file)
    #4. import books
    import_books(in_db_file, out_db_file)
    #5. logging
    logging.info("importLibrary: FINISHED")

def test_import_library(delete_out_db=False):
    in_db_file=['tests/celine.db',
                'tests/celine2.db',
                'tests/celine3.db',
                'tests/helena.db','tests/helena2.db',
                'tests/jerome.db','tests/mine.db']
    #in_db_file=['tests/celine2.db']
    in_sql_script='data/newDB.sql'
    out_db_file='data/test.db'
    logging.basicConfig(filename="test_bibou.log", level=logging.INFO,
                    format='%(asctime)s %(message)s',
                    datefmt='%d/%m/%Y %H:%M:%S')
    if os.path.isfile(out_db_file) and delete_out_db:
        os.remove(out_db_file)
    for inDd in in_db_file:
        import_library(in_sql_script,inDd,out_db_file)

def test_import_users(delete_out_db=False):
    xls_file="tests/FICHIER_ELEVES.xls"
    out_db_file='data/test.db'
    if os.path.isfile(out_db_file) and delete_out_db:
        os.remove(out_db_file)
    import_users(xls_file, out_db_file)