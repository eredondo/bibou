#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bibou new book: graphic user interface for cerating new books in the library
Created on Mon Nov 16 

@author: redondo
"""

if __package__ is None or __package__ == "":
    import SQLconnector as b_sq
    import external_data_tools as b_ext
    from common import *
else:
    from . import SQLconnector as b_sq
    from . import external_data_tools as b_ext
    from .common import *

#TODO modify users
class UserSearch:
    def __init__(self, config, edition_mode=False):
        self.current_user = []
        self.config = config
        self.edition_mode = edition_mode
        self.database_file = self.config['Data']['database_file']
        self.sql = b_sq.SQLconnector(self.database_file)
        self.main_loop()
        #if isinstance(self.selected_user,int):
        #    self.current_user = self.table_data[self.selected_user]
    def main_loop(self):
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            #self.update_main_window()
            if event is None or event == 'exit':
                self.selected_user = []
                self.current_user = []
                break
            if event == 'select':
                self.selected_user = values['main_table']
                if self.selected_user:
                    self.selected_user = self.selected_user[0]
                    self.current_user = self.table_data[self.selected_user]
                    break
                PopupErreur("ERREUR: Il faut sélectionner un élève",title="",font=big_font)
            if event == 'erase':
                print("event is erase")
                self.selected_user = []
                self.current_user = []
                self.main_window.Element("fil_fir").Update('')
                self.main_window.Element("fil_las").Update('')
                self.main_window.Element("fil_lev").Update('')
                self.filter_user_list()
                self.update_main_window()
            #if event == 'level':
            #    print("faire un popup, avec input = level")
            if event == 'export':
                self.export_user_dialog()
            if event == 'import':
                self.import_user_dialog()
                self.filter_user_list()
                self.update_main_window()
            if event == 'oldnew2':
                user_first = self.main_window.Element("fil_fir").Get()
                user_last = self.main_window.Element("fil_las").Get()
                user_level = self.main_window.Element("fil_lev").Get()
                if user_first and user_last and user_level:
                    answer = PopupOuiNon("Créer nouvel élève:\n"
                                         + user_first
                                         + " "
                                         + user_last
                                         + "\nNiveau: "
                                         + user_level,
                                         title="Nouvel élève")
                    if answer == "OUI":
                        self.sql.ins_user(user_first, user_last, user_level)
                else:
                    PopupErreur("Tous les champs sont obligatoires",
                                title="Nouvel élève")
                self.filter_user_list()
                self.update_main_window()
            if event == 'new':
                    self.main_window.Hide()
                    self.new_user_loop()
                    self.update_main_window()
                    self.main_window.UnHide()
            if event == 'modify':
                print("Let's change things!")
                self.selected_user = values['main_table']
                if self.selected_user:
                    self.selected_user = self.selected_user[0]
                    self.current_user = self.table_data[self.selected_user]
                    self.main_window.Hide()
                    self.user_loop()
                    self.update_main_window()
                    self.main_window.UnHide()
                else:
                    PopupErreur("ERREUR: Il faut sélectionner un élève",title="",font=big_font)
            if event == 'main_table':
                self.selected_user = values['main_table'][0] if values['main_table'] else []
                self.current_user = self.table_data[self.selected_user] if self.selected_user else [[], [], [], []]
                #self.main_window.Element("fil_fir").Update(self.current_user[1])
                #self.main_window.Element("fil_las").Update(self.current_user[2])
                #self.main_window.Element("fil_lev").Update(self.current_user[3])
            if event.startswith("fil"):
                print("filtrer")
                self.selected_user = []
                self.current_user = []
                self.filter_user_list()
                self.update_main_window()
            print(event, values)
        self.main_window.Close()
    def make_main_window(self):
        self.table_data = self.sql.list_users()
        headers = ['ID', 'PRENOM', 'NOM', 'NIVEAU']
        col_ws = [5, 20, 20, 8]
        #TABLE
        TABLE = make_sg_table("main_table", self.table_data,
                              headers, col_ws, enable_events=True, num_rows=20)
        #buttons
        bt_select = sg.Button(key="select", button_text="SELECTIONNER",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_export = sg.Button(key="export", button_text="EXPORTER LISTE",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_import = sg.Button(key="import",
                                    button_text="IMPORTER\nFICHIER D'ELEVES",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_erase = sg.Button(key="erase", button_text="EFFACER FORMULAIRE",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_exit = sg.Button(key="exit", button_text="SORTIR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        #bt_level = sg.Button(key="level", button_text="CHANGER DE NIVEAU",
        #                      font=medium_font, border_width=1,
        #                      size=(25, 3), pad=(0, 0))
        bt_new = sg.Button(key="new", button_text="NOUVEL UTILISATEUR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_modify = sg.Button(key="modify", button_text="MODIFIER UTILISATEUR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        #input texts
        input_fir = sg.InputText(key="fil_fir", enable_events=True, do_not_clear=True)
        input_las = sg.InputText(key="fil_las", enable_events=True, do_not_clear=True)
        input_lev = sg.InputText(key="fil_lev", enable_events=True, do_not_clear=True)

        #Texts:
        tx = sg.Text("RECHERCHE D'ELEVES (" + str(len(self.table_data)) + "):",
                     key="tx_heading",
                     size=(35, 1),
                     font=big_font,
                     background_color=skyblue)
        tx_fir = sg.Text('PRENOM', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #title filter
        tx_las = sg.Text('NOM', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #lastname filter
        tx_lev = sg.Text('NIVEAU', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #firstname filter
        #MIDDLE COLUMN
        im_left,im_right,im_top,im_top2 = take_images()
        if self.edition_mode:
            col = [[im_top2],
                   [tx],
                   [TABLE],
                   [im_top2],
                   [tx_fir, input_fir],
                   [tx_las, input_las],
                   [tx_lev, input_lev],
                   [im_top2],
                   [bt_modify, bt_export, bt_erase],
                   [bt_new, bt_import, bt_exit]]
        else:
            col = [[im_top2],
                   [tx],
                   [TABLE],
                   [im_top2],
                   [tx_fir, input_fir],
                   [tx_las, input_las],
                   [tx_lev, input_lev],
                   [im_top2],
                   [bt_select, bt_erase, bt_exit]]
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.main_window = sg.Window("RECHERCHE D'ELEVE", size=(1024, 768),
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     background_color=skyblue,
                                     margins=(0, 0)).Layout(layout)
    def update_main_window(self):
        TABLE = self.main_window.Element("main_table")
        TABLE.Update(values=self.table_data)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE.AlternatingRowColor = 'lightblue'
        TABLE.RowColor = 'blue'
        TX = self.main_window.Element("tx_heading")
        TX.Update(value="RECHERCHE D'ELEVES (" + str(len(self.table_data)) + "):")
    def filter_user_list(self):
        f_firstname = self.main_window.Element("fil_fir").Get()
        f_lastname = self.main_window.Element("fil_las").Get()
        f_level = self.main_window.Element("fil_lev").Get()
        if f_level.isnumeric():
            f_level = int(f_level)
        else:
            f_level = 0
        #(ID,FIRSTNAME,LASTNAME,LEVEL)
        self.table_data = self.sql.list_users(firstname=f_firstname,
                                lastname=f_lastname,level=f_level)
    def export_user_list(self, path_name):
        xls_book = xlwt.Workbook()
        xls_sheet = xls_book.add_sheet('User export')
        #first_line
        #(ID,FIRSTNAME,LASTNAME,LEVEL)
        xls_sheet.write(0, 0, 'ID')
        xls_sheet.write(0, 1, 'FIRSTNAME')
        xls_sheet.write(0, 2, 'LASTNAME')
        xls_sheet.write(0, 3, 'LEVEL')

        #next lines
        for i_row,user in enumerate(self.table_data):
            for i_col,item in enumerate(user):
                xls_sheet.write(i_row+1, i_col, item)

        xls_book.save(path_name)
    def export_user_dialog(self):
        bt_export = sg.FileSaveAs(key="export",
                                  target='input',
                                  button_text="Choisir fichier...",
                                  font=medium_font, #border_width=1,
                                  #size=(25, 3), pad=(0, 0),
                                  enable_events=True,
                                  file_types=(('Excel 97','*.xls'),))
        layout = [[sg.T('Enregistrer sous...')],
                  [sg.In(key='input')],
                  [bt_export, sg.OK()]]
        user_dialog_window = sg.Window("EXPORTER LA LISTE D'ELEVES",
                                       element_padding=((0, 0), (0, 0)),
                                       element_justification="center",
                                       #background_color=skyblue,
                                       margins=(0, 0)).Layout(layout)
        while True:
            event, values = user_dialog_window.Read()
            if event is None or event == 'exit':
                self.selected_user = []
                self.current_user = []
                break
            if event == 'OK':
                path_name = values['input']
                self.export_user_list(path_name)
                PopupInfo("Liste exportée sous\n" + path_name,
                          title="Information")
                break
        user_dialog_window.Close()
    def import_user_dialog(self):
        bt_import = sg.FileBrowse(key="export",
                                  target='input',
                                  button_text="Choisir fichier...",
                                  font=medium_font, #border_width=1,
                                  #size=(25, 3), pad=(0, 0),
                                  enable_events=True,
                                  file_types=(('Excel 97','*.xls'),))
        layout = [[sg.T('Ouvrir un fichier *.xls...')],
                  [sg.In(key='input')],
                  [bt_import, sg.OK()]]
        user_dialog_window = sg.Window("IMPORTER FICHIER D'ELEVES",
                                       element_padding=((0, 0), (0, 0)),
                                       element_justification="center",
                                       #background_color=skyblue,
                                       margins=(0, 0)).Layout(layout)
        while True:
            event, values = user_dialog_window.Read()
            if event is None or event == 'exit':
                self.selected_user = []
                self.current_user = []
                break
            if event == 'OK':
                path_name = values['input']
                n_cols = b_ext.test_xls_user_file(path_name) if path_name else 0
                if n_cols==4:
                    print("faire un update avec", path_name)
                    b_ext.update_users(path_name,self.database_file)
                    PopupInfo("Liste importée de\n" + path_name,
                              title="Information")
                elif n_cols==3:
                    print("faire un import avec", path_name)
                    b_ext.import_users(path_name,self.database_file)
                    PopupInfo("Liste importée de\n" + path_name,
                              title="Information")
                else:
                    print("rien à faire")
                break
        user_dialog_window.Close()
    def user_loop(self):
        self.make_user_window()
        while True:
            event, values = self.user_window.Read()
            if event is None or event == 'Exit' or event == 'retour':
                break
            if event == "update":
                self.change_user()
                self.update_user_window()
            if event == "undo":
                self.update_user_window()
            if event.startswith("inp") or event == "update" or event == "undo":
                self.changed_user_info()
            print(event, values)
        self.user_window.Close()
    def make_user_window(self):
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        current_user = self.current_user
        #Buttons
        bt_update = sg.Button(key="update", button_text="Mettre à jour")
        bt_undo = sg.Button(key="undo", button_text="Défaire modifications")
        bt_exit = sg.Button(key="retour", button_text="Retour")

        #Inputtext Elements
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        input_id = sg.InputText(current_user[0], size=(30, 1),
                                disabled=True,
                                key="inp_id", enable_events=True, do_not_clear=True)
        input_las = sg.InputText(current_user[1], size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_las", enable_events=True, do_not_clear=True)
        input_fir = sg.InputText(current_user[2], size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_fir", enable_events=True, do_not_clear=True)
        input_lev = sg.InputText(current_user[3], size=(30, 1),
                                  disabled=not self.edition_mode,
                                  key="inp_lev", enable_events=True, do_not_clear=True)
        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        #List Elements
        #Text Elements
        tx_id = sg.Text('ID', key="tx_id", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #ID
        tx_las = sg.Text('NOM', key="tx_las", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #LASTNAME
        tx_fir = sg.Text('PRENOM', key="tx_fir", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #FIRSTNAME
        tx_lev = sg.Text('NIVEAU', key="tx_lev", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #LEVEL
        if self.edition_mode:
            col = [[im_top2],
                   [tx_id, input_id],
                   [tx_las, input_las],
                   [tx_fir, input_fir],
                   [tx_lev, input_lev],
                   [im_top2],
                   [bt_update, bt_undo, bt_exit]]
        else:
            col = [[im_top2],
                   [tx_id, input_id],
                   [tx_las, input_las],
                   [tx_fir, input_fir],
                   [tx_lev, input_lev],
                   [im_top2],
                   [bt_exit]]
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.user_window = sg.Window("Fiche d'élève", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
        self.update_user_window()
    def update_user_window(self):
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        self.current_user = self.table_data[self.selected_user]
        current_user = self.current_user
        self.user_window.Element("inp_id").Update(current_user[0])
        self.user_window.Element("inp_fir").Update(current_user[1])
        self.user_window.Element("inp_las").Update(current_user[2])
        self.user_window.Element("inp_lev").Update(current_user[3])
    def changed_user_info(self):
        '''
            Detect any change in user form and mark them in red color
        '''
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        current_user = self.current_user
        print(current_user)
        #ID
        id_user = int(self.user_window.Element("inp_id").Get())
        if id_user != current_user[0]:
            tx_color='red'
        else:
            tx_color='black'
        self.user_window.Element("tx_id").Update(text_color=tx_color)
        #FIRSTNAME
        firstname = self.user_window.Element("inp_fir").Get()
        if firstname != current_user[1]:
            tx_color='red'
        else:
            tx_color='black'
        self.user_window.Element("tx_fir").Update(text_color=tx_color)
        #LASTNAME
        lastname = self.user_window.Element("inp_las").Get()
        if lastname != current_user[2]:
            tx_color='red'
            print(lastname + " is different than " + current_user[1])
        else:
            tx_color='black'
        self.user_window.Element("tx_las").Update(text_color=tx_color)
        #LEVEL
        level_user_tx = self.user_window.Element("inp_lev").Get()
        level_user = int(level_user_tx) if level_user_tx.isdigit() else []
        if level_user != current_user[3]:
            tx_color='red'
        else:
            tx_color='black'
        self.user_window.Element("tx_lev").Update(text_color=tx_color)
    def change_user(self):
        '''
            Take values in form and update book info in database
        '''
        print("Let's change things!")
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        id_user = int(self.user_window.Element("inp_id").Get())
        firstname = self.user_window.Element("inp_fir").Get()
        lastname = self.user_window.Element("inp_las").Get()
        level = self.user_window.Element("inp_lev").Get()
        print("ID is " + str(id_user))
        print("FIRSTNAME is " + firstname)
        print("LASTNAME is " + lastname)
        print("LEVEL is " + level)
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        current_user = self.current_user
        if id_user != current_user[0]:
            print("ERROR in id_user")
            print("id_user is " + id_user + ", but current_user[0] is " + str(current_user[0]))
        if firstname != current_user[1]:
            print("update FIRSTNAME")
            self.sql.update_by_id(id_user, "USER", "FIRSTNAME", firstname)
        if lastname != current_user[2]:
            print("update LASTNAME")
            self.sql.update_by_id(id_user, "USER", "LASTNAME", lastname)
        if level.isdigit():
            if int(level) != current_user[3]:
                print("update LEVEL")
                self.sql.update_by_id(id_user, "USER", "LEVEL", level)
        else:
            PopupErreur("ERREUR: Le niveau doit être un chiffre",title="",font=big_font)
        self.table_data = self.sql.list_users()
    def new_user_loop(self):
        self.make_new_user_window()
        while True:
            event, values = self.new_user_window.Read()
            if event is None or event == 'Exit' or event == 'retour':
                break
            if event == "write":
                self.create_user()
                break
            print(event, values)
        self.new_user_window.Close()
    def make_new_user_window(self):
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        #Buttons
        bt_write = sg.Button(key="write", button_text="Créer utilisateur")
        bt_exit = sg.Button(key="retour", button_text="Retour")
        #Inputtext Elements
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        #input_id = sg.InputText(1000, size=(30, 1),
        #                        disabled=True,
        #                        key="inp_id", enable_events=True, do_not_clear=True)
        input_las = sg.InputText('', size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_las", enable_events=True, do_not_clear=True)
        input_fir = sg.InputText('', size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_fir", enable_events=True, do_not_clear=True)
        input_lev = sg.InputText('', size=(30, 1),
                                  disabled=not self.edition_mode,
                                  key="inp_lev", enable_events=True, do_not_clear=True)
        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        #List Elements
        #Text Elements
        #tx_id = sg.Text('ID', key="tx_id", size=(10, 1), font=medium_font,
        #                 background_color=skyblue)  #ID
        tx_las = sg.Text('NOM', key="tx_las", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #LASTNAME
        tx_fir = sg.Text('PRENOM', key="tx_fir", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #FIRSTNAME
        tx_lev = sg.Text('NIVEAU', key="tx_lev", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #LEVEL
        col = [[im_top2],
        #       [tx_id, input_id],
               [tx_las, input_las],
               [tx_fir, input_fir],
               [tx_lev, input_lev],
               [im_top2],
               [bt_write, bt_exit]]
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.new_user_window = sg.Window("Création d'un nouvel utilisateur", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
    def create_user(self):
        '''
            Take values in form and insert new user into user table in database
        '''
        print("Let's create things!")
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        #id_user = int(self.new_user_window.Element("inp_id").Get())
        firstname = self.new_user_window.Element("inp_fir").Get()
        lastname = self.new_user_window.Element("inp_las").Get()
        level = self.new_user_window.Element("inp_lev").Get()
        #print("ID is " + str(id_user))
        print("FIRSTNAME is " + firstname)
        print("LASTNAME is " + lastname)
        print("LEVEL is " + level)
        #'ID','LASTNAME','FIRSTNAME','LEVEL'
        #TODO checkup other fields correctness
        if not firstname:
            PopupErreur("ERREUR: Le prénom doit être renseigné",title="",font=big_font)
        elif not lastname:
            PopupErreur("ERREUR: Le nom doit être renseigné",title="",font=big_font)
        elif not level.isdigit():
            PopupErreur("ERREUR: Le niveau doit être un chiffre",title="",font=big_font)
        else:
            answer = PopupOuiNon("Créer nouvel élève:\n"
                                 + firstname
                                 + " "
                                 + lastname
                                 + "\nNiveau: "
                                 + level,
                                 title="Nouvel élève")
            if answer == "OUI":
                self.sql.ins_user(firstname, lastname, level)
        self.table_data = self.sql.list_users()
if __name__ == '__main__':
    import sys
    #change curdir to parent folder of catalog.py:
    #print("Maintenant je suis à:", os.getcwd())
    module_path , module_file = os.path.split(__file__)
    parent_path = os.path.join(module_path,'..')
    os.chdir(parent_path)

    #print("Maintenant je suis à:", os.getcwd())

    logging.basicConfig(filename="bibou.log", level=logging.INFO,
                        format='%(asctime)s %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    config = configparser.ConfigParser()
    if not os.path.isfile('settings.cfg'):
        print("Settings file not found")
        sys.exit()
    print("On a trouvé le fichier settings")
    config.read('settings.cfg')
    UserSearch(config,True)
else:
    pass
