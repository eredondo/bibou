import sqlite3
import datetime
'''
    SQLConnector for new database format inspired from Android App
'''
#TODO unit test for every method
#TODO logging capability
#TODO finalize translation and remove unused methods
class SQLconnector:
    def __init__(self, SqliteFile):
        self.DBFile = SqliteFile
        #self.connect(SqliteFile)
        #self.con = self.connect()
        #self.c = self.con.cursor()
#    def dict_factory(cursor, row):
#        d = {}
#        for idx, col in enumerate(cursor.description):
#            d[col[0]] = row[idx]
#        return d
    def connect(self):
        print(self.DBFile)
        self.con = sqlite3.connect(self.DBFile)
        #self.con.row_factory = dict_factory
        self.c = self.con.cursor()
    def disconnect(self):
        self.con.close()
    def list_books(self, ref="", title="", author_firstname="",author_lastname="", editor="", abstract="", keywords="", state=-10):
        '''
            select books in table CATALOG with filters 
        '''
        self.connect()
        ref_filter = " where REF like '%" + ref + "%'"
        firstname_filter = " and FIRSTNAME like '%" + author_firstname.replace("'","''") + "%'"
        lastname_filter = " and LASTNAME like '%" + author_lastname.replace("'","''") + "%'"
        editor_filter = " and EDITOR like '%" + editor.replace("'","''") + "%'"
        title_list = [t for t in title.split(" ") if t]
        title_filter = [" and TITLE like '%" + t.replace("'","''") + "%'" for t in title_list]
        abstract_list = [t for t in abstract.split(" ") if t]
        abstract_filter = [" and abstract like '%" + t.replace("'","''") + "%'" for t in abstract_list]
        keywords_list = [t for t in keywords.split(" ") if t]
        keywords_filter = [" and KEYWORDS like '%" + t.replace("'","''") + "%'" for t in keywords_list]
        if state>-10:
            state_filter = " and STATE=" + str(state)
        else:
            state_filter = ""
        sql='select * from CATALOG' + ref_filter + firstname_filter + lastname_filter + editor_filter \
+ ''.join(title_filter) + ''.join(abstract_filter) + ''.join(keywords_filter) + state_filter
        #print(sql) #logging debug?
        self.c.execute(sql)
        r = self.c.fetchall()
        self.disconnect()
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        return r
    def get_by_id(self, id, table):
        self.connect()
        self.c.execute('select * from '+ table +' where ID='+str(id))
        r = self.c.fetchall()
        self.disconnect()
        if not r:
            return None
        if len(r) > 1:
            print("ERROR!")
        return r[0]
    def get_author_by_id(self, id):
        return self.get_by_id(id, 'AUTHOR')
    def get_editor_by_id(self, id):
        return self.get_by_id(id, 'EDITOR')
    def get_book_by_id(self, id):
        return self.get_by_id(id, 'BOOK')
    def get_book_by_ref(self, ref):
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        self.connect()
        self.c.execute("select * from BOOK where REF='"+ref+"'")
        r = self.c.fetchall()
        self.disconnect()
        if not r:
            return None
        if len(r) > 1:
            print("ERROR!")
        return r[0]
    def get_user_by_id(self, id):
        return self.get_by_id(id, 'USER')
    def get_loan_by_id(self, id):
        return self.get_by_id(id, 'LOAN')
    def update_by_id(self, id, table, field, value):
        '''
            Update a row in a table
        '''
        self.connect()
        if isinstance(value,str):
            sql = "UPDATE " + table \
                  + " SET " + field + " = '" + value \
                  + "' WHERE ID = " + str(id)
        else:# is numeric
            sql = "UPDATE " + table \
                  + " SET " + field + " = " + str(value) \
                  + " WHERE ID = " + str(id)
        #print(sql)
        try:
            self.con.execute(sql)
            self.con.commit()
        except sqlite3.Error as e:
            print("ERROR:update_by_id:(" + e.args[0] + ")")
        #print("update OK")
        self.disconnect()
    def list_authors(self, firstname="", lastname=""):
        self.connect()
        lastname_filter = " where LASTNAME like '%" + lastname.replace("'","''") + "%'"
        firstname_filter = " and FIRSTNAME like '%" + firstname.replace("'","''") + "%'"
        self.c.execute('select * from AUTHOR'+lastname_filter+firstname_filter)
        r = self.c.fetchall()
        self.disconnect()
        return r
    def list_authors_book_count(self, firstname="", lastname=""):
        self.connect()
        lastname_filter = " where LASTNAME like '%" + lastname.replace("'","''") + "%'"
        firstname_filter = " and FIRSTNAME like '%" + firstname.replace("'","''") + "%'"
        self.c.execute('select * from AUTHOR_BOOK_COUNT'
                       + lastname_filter + firstname_filter)
        #ID, FIRSTNAME, LASTNAME
        r = self.c.fetchall()
        self.disconnect()
        return r
    def list_editors(self, name=""):
        self.connect()
        nameFilter = " where NAME like '%" + name.replace("'","''") + "%'"
        #print(nameFilter)
        self.c.execute('select * from EDITOR'+nameFilter)
        r = self.c.fetchall()
        self.disconnect()
        return r
    def list_editors_book_count(self, name=""):
        self.connect()
        nameFilter = " where NAME like '%" + name.replace("'","''") + "%'"
        #print(nameFilter)
        self.c.execute('select * from EDITOR_BOOK_COUNT'+nameFilter)
        r = self.c.fetchall()
        self.disconnect()
        return r
    def list_users(self, firstname="", lastname="", level=0):
        self.connect()
        lastname_filter = " where LASTNAME like '%" + lastname.replace("'","''") + "%'"
        firstname_filter = " and FIRSTNAME like '%" + firstname.replace("'","''") + "%'"
        if not level:
            level_filter = ""
        else:
            level_filter = " and LEVEL="+str(level)
        self.c.execute('select * from USER'+lastname_filter+firstname_filter+level_filter)
        r = self.c.fetchall()
        self.disconnect()
        return r
#    def listBooks(self, title="", idAuthor=0, state=-1, keywords=[]):
#        self.connect()
#        titleFilter = " where TITLE like '%" + title + "%'"
#        if not idAuthor:
#            authorFilter = ""
#        else:
#            authorFilter = " and ID_AUTHOR="+str(idAuthor)
#        if state < 0:
#            stateFilter = ""
#        else:
#            stateFilter = " and STATE="+str(state)
#        keyFilter = ""
#        for k in keywords:
#            keyFilter = keyFilter+" and KEYWORDS like '%" + k + "%'"
#        self.c.execute('select * from BOOK'+titleFilter+authorFilter+stateFilter+keyFilter)
#        r = self.c.fetchall()
#        self.disconnect()
#        return r
    def list_loans(self, id_book=0, id_user=0,
                   from_date_loan="2000-01-01", to_date_loan="3000-01-01",
                   from_date_return="2000-01-01", to_date_return="3000-01-01"):
        if not id_book:
            book_filter = ""
        else:
            book_filter = " and ID_BOOK="+str(id_book)
        if not id_user:
            user_filter = ""
        else:
            user_filter = " and ID_USER="+str(id_user)
        self.connect()
        self.c.execute("select * from LOAN where DATE_LOAN>='" + from_date_loan
                       + "' and DATE_LOAN<='" + to_date_loan
                       + "' and DATE_RETURN>='" + from_date_return
                       + "' and DATE_RETURN<='" + to_date_return
                       + "'" + bookFilter + userFilter + ";")
        r = self.c.fetchall()
        self.disconnect()
        return r
    def list_full_loans_info(self, ref_book="", tit_book="", state_book=-10,
                   id_user=0,firstname_user="", lastname_user="", level_user=0,
                   returned=-1,
                   from_date_loan="2000-01-01", to_date_loan="3000-01-01",
                   from_date_return="2000-01-01", to_date_return="3000-01-01"):
        if not level_user:
            level_filter = ""
        else:
            level_filter = " and LEVEL="+str(level_user)
        if not id_user:
            user_filter = ""
        else:
            user_filter = " and ID_USER="+str(id_user)
        if state_book>-10:
            state_filter = " and STATE=" + str(state_book)
        else:
            state_filter = ""
        if returned>-1:
            returned_filter = " and RETURNED=" + str(returned)
        else:
            returned_filter = ""
        ref_filter = " and REF like '%" + ref_book + "%'"
        title_list = [t for t in tit_book.split(" ") if t]
        title_filter = [" and TITLE like '%" + t.replace("'","''") + "%'" for t in title_list]
        firstname_filter = " and FIRSTNAME like '%" + firstname_user.replace("'","''") + "%'"
        lastname_filter = " and LASTNAME like '%" + lastname_user.replace("'","''") + "%'"
        
        self.connect()
        self.c.execute("select * from LOAN3 where DATE_LOAN>='" + from_date_loan
                       + "' and DATE_LOAN<='" + to_date_loan
                       + "' and DATE_RETURN>='" + from_date_return
                       + "' and DATE_RETURN<='" + to_date_return
                       + "'" + level_filter + user_filter
                       + firstname_filter + lastname_filter
                       + ref_filter + ''.join(title_filter) + state_filter + returned_filter
                       + ";")
        r = self.c.fetchall()
        self.disconnect()
        #ID, DATE_LOAN, DATE_RETURN, RETURNED [0 1 2 3]
        #REF, TITLE, STATE                    [4 5 6]
        #ID_USER, FIRSTNAME, LASTNAME, LEVEL  [7 8 9 10]
        return r
    def list_loans_by_user(self, id_user=0, returned=0):
        self.connect()
        #ID, ID_BOOK, ID_USER, DATE_LOAN, DATE_RETURN, REF, TITLE, STATE
        sql = "select LOAN.ID, LOAN.ID_BOOK, LOAN.ID_USER, "\
              + "LOAN.DATE_LOAN, LOAN.DATE_RETURN, "\
              + "BOOK.REF, BOOK.TITLE, BOOK.STATE "\
              + "from LOAN join BOOK "\
              + "on LOAN.ID_BOOK=BOOK.ID "\
              + "where LOAN.RETURNED=" + str(returned) + " "\
              + "and LOAN.ID_USER=" + str(id_user) + ";"
        print(sql)
        self.c.execute(sql)
        r = self.c.fetchall()
        self.disconnect()
        return r
    def ins_editor(self, name):
        self.connect()
        self.c.execute("select * from EDITOR where NAME='" + name.replace("'","''") + "';")
        r = self.c.fetchall()
        if not r:
            self.c.execute("insert into EDITOR (NAME) values('"+name.replace("'","''")+"');")
            self.con.commit()
            self.disconnect()
            return 0
        else:
            self.disconnect()
            print("ins_editor: Error, editor already exists")
            return -1
    def ins_author(self, firstname, lastname):
        self.connect()
        self.c.execute("select * from AUTHOR where FIRSTNAME='" +firstname.replace("'","''")+ "' and LASTNAME='" + lastname.replace("'","''")+ "';")
        r = self.c.fetchall()
        if not r:
            self.c.execute("insert into AUTHOR (FIRSTNAME,LASTNAME) values('"+firstname.replace("'","''")+"','"+lastname.replace("'","''")+"');")
            self.con.commit()
            self.disconnect()
            return 0
        else:
            self.disconnect()
            print("ins_author: Error, author already exists")
            return -1
    def ins_user(self, firstname, lastname, level):
        self.connect()
        self.c.execute("select * from USER where FIRSTNAME='" +firstname.replace("'","''")+ "' and LASTNAME='" + lastname.replace("'","''")+ "';")
        r = self.c.fetchall()
        if not r:
            self.c.execute("insert into USER (FIRSTNAME,LASTNAME,LEVEL) values('"+firstname.replace("'","''")+"','"+lastname.replace("'","''")+"','"+level+"');")
            self.con.commit()
            self.disconnect()
            return 0
        else:
            self.disconnect()
            print("ins_user: Error, user already exists")
            return -1
    def ins_book(self, fields):
        '''
            fields is a dictionnary with the following fields: TITLE,ISBN,ID_AUTHOR,ID_OTHER_AUTHORS,
                   ID_EDITOR,ABSTRACT,KEYWORDS,COMMENTS,COVER_PATH,
                   CATEGORIES,PAGES,PUBLISHED_DATE,STATE
        '''
        #FIXME this function does not work
        if not self.author_exists(fields['ID_AUTHOR']):
            print("insBook: author does not exist")
            return -1
        if not self.editor_exists(fields['ID_EDITOR']):
            print("insBook: editor does not exist")
            return -1
        INSTR_SQL = "insert into BOOK (REF,TITLE,ISBN,ID_AUTHOR,ID_EDITOR," +\
                    "ABSTRACT,KEYWORDS,STATE) values('" +\
                    fields['REF'] + "' , '" +\
                    fields['TITLE'] + "' , '" +\
                    fields['ISBN'] + "' , " +\
                    str(fields['ID_AUTHOR']) + " , " +\
                    str(fields['ID_EDITOR']) + " , '" +\
                    fields['ABSTRACT'] + "' , '" +\
                    fields['KEYWORDS'] + "', 0);"
        print(INSTR_SQL)
        #return 0
        self.connect()
        self.c.execute(INSTR_SQL)
        self.con.commit()
        self.disconnect()
        return 0
    def lend_book(self, id_book, id_user, date_loan=str(datetime.date.today()),
                date_return=str(datetime.date.today()+datetime.timedelta(14))):
        if not self.book_exists(id_book):
            print("lend_book: the book does not exist")
            return -1
        if not self.user_exists(id_user):
            print("lend_book: the user does not exist")
            return -2
        if not self.book_available(id_book):
            print("lend_book:the book is not available")
            return -3
        self.connect()
        self.c.execute("select * from LOAN where ID_BOOK=" +str(id_book)+ \
                      " and ID_USER=" + str(id_user)+ " and DATE_LOAN='" +date_loan+\
                       "' and DATE_RETURN='" +date_return+ "' and RETURNED=0;")
        r = self.c.fetchall()
        if not r:
            self.c.execute("insert into LOAN (ID_BOOK,ID_USER,DATE_LOAN,DATE_RETURN,RETURNED) values("+str(id_book)+","+str(id_user)+",'"+date_loan+"','"+date_return+"',0);")
            self.con.commit()
            self.c.execute("update BOOK set STATE = 1 where id="+str(id_book)+";")
            self.con.commit()
            self.disconnect()
            return 0
        else:
            self.disconnect()
            print("lend_book: ERROR, this loan already exists")
            return -4
    def return_book(self, id_loan, date_return=str(datetime.date.today())):
        # TODO update date_return?
        # TODO add bool returned book?
        self.connect()
        self.c.execute("select ID_BOOK from LOAN where ID=" +str(id_loan)+" and RETURNED=0;")
        r = self.c.fetchall()
        if not r:
            self.disconnect()
            print("return_book: error, the loan does not exist")
            return -1
        else:
            id_book=r[0][0]
            self.c.execute("update BOOK set STATE = 0 where ID="+str(id_book)+";")
            self.con.commit()
            self.c.execute("update LOAN set RETURNED = 1 where ID="+str(id_loan)+";")
            self.con.commit()
            self.c.execute("update LOAN set DATE_RETURN = '" +date_return+ "' where ID="+str(id_loan)+";")
            self.con.commit()
            self.disconnect()
            return 0
    def prolonge_pret(self,id, date_retour=str(datetime.date.today()+datetime.timedelta(14))):
        self.connect()
        self.c.execute("select * from PRET where id=" +str(id)+";")
        r = self.c.fetchall()
        if not r:
            self.disconnect()
            print("retour_pret: erreur, le pret n'existe pas")
            return -1
        else:
            self.c.execute("update PRET set date_retour = '"+date_retour+"' where id="+str(id)+";")
            self.con.commit()
            self.disconnect()
            return 0
    def book_available(self,id_book):
        self.connect()
        self.c.execute("select * from BOOK where ID=" +str(id_book)+" and STATE=0;")
        r = self.c.fetchall()
        if not r:
            return False
        else:
            return True
    def book_exists(self,id_book):
        self.connect()
        self.c.execute("select * from BOOK where ID=" +str(id_book)+";")
        r = self.c.fetchall()
        self.disconnect()
        if not r:
            return False
        else:
            return True
    def user_exists(self,id):
        self.connect()
        self.c.execute("select * from USER where ID=" +str(id)+";")
        r = self.c.fetchall()
        self.disconnect()
        if not r:
            return False
        else:
            return True
    def author_exists(self,id):
        self.connect()
        self.c.execute("select * from AUTHOR where ID=" +str(id)+";")
        r = self.c.fetchall()
        self.disconnect()
        if not r:
            return False
        else:
            return True
    def editor_exists(self,id):
        self.connect()
        self.c.execute("select * from EDITOR where ID=" +str(id)+";")
        r = self.c.fetchall()
        self.disconnect()
        if not r:
            return False
        else:
            return True

