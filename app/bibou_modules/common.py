#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bibou common: common functions and constants
Created on Nov 22 17:33 2019

@author: redondo
"""
import os
import datetime
import configparser
import logging
import xlwt
import PySimpleGUI as sg

#fonts
big_font = "ARIAL 15"
medium_font = "ARIAL 10"
small_font = "ARIAL 8"

#colors
white="#FFFFFF"
black="#000000"
orange0="#ff9800"
orange1="#e65100"
red0="#F44336"
red1="#b71c1c"
blue0="#64b5f6"
blue1="#0d47a1"
skyblue="#87DECD"
green0="#66bb6a"
green1="#1b5e20"
violet0="#ba68c8"
violet1="#4a148c"
pink0="#f06292"
pink1="#ad1457"

def take_images():
    #Images
    im_left = sg.Image("images/left.png", background_color=skyblue)
    im_right = sg.Image("images/right.png", background_color=skyblue)
    im_top = sg.Image("images/top.png", background_color=skyblue)
    im_top2 = sg.Image("images/top2.png", background_color=skyblue)
    return im_left,im_right,im_top,im_top2

def PopupOuiNon(message,title="",font=big_font):
    return sg.Popup(message,title=title, font=font,custom_text=("OUI","NON"))

def PopupInput(message,title="",font=big_font):
    return sg.PopupGetText(message,title=title, font=font)

def PopupErreur(message,title="",font=big_font):
    sg.Popup(message,title=title, font=font, custom_text="OK",
                  auto_close=True, auto_close_duration=2)

def PopupInfo(message,title="",font=medium_font):
    return sg.Popup(message,title=title, font=font,custom_text="OK")

def make_sg_table(key, values,headings,col_widths,
                  def_col_width=5,
                  max_col_width=25,
                  auto_size_columns=False,
                  vertical_scroll_only=True,
                  justification='left',
                  bind_return_key=False,#enable double click or return event
                  enable_events=False,#disable single click event
                  pad=(1, 1),
                  font=medium_font,
                  alternating_row_color='lightblue',
                  select_mode=sg.TABLE_SELECT_MODE_BROWSE,
                  num_rows=5):
    """
    Dummy function to set the same default values for all PySimpleGUI tables
    """
    return sg.Table(key=key,values=values,headings=headings,col_widths=col_widths,
                    def_col_width=def_col_width,
                    max_col_width=max_col_width,
                    auto_size_columns=auto_size_columns,
                    vertical_scroll_only=vertical_scroll_only,
                    justification=justification,
                    bind_return_key=bind_return_key,#enable double click or return event
                    enable_events=enable_events,#disable single click event
                    pad=pad,
                    font=font,
                    alternating_row_color=alternating_row_color,
                    select_mode=select_mode,
                    num_rows=num_rows)