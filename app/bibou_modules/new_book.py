#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bibou new book: graphic user interface for cerating new books in the library
Created on Mon Nov 16 

@author: redondo
"""

if __package__ is None or __package__ == "":
    import SQLconnector as b_sq
    from common import *
    import author_search as b_as
    import editor_search as b_es
else:
    from . import SQLconnector as b_sq
    from .common import *
    from . import author_search as b_as
    from . import editor_search as b_es

class NewBook:
    '''
        New book form
    '''
    def __init__(self,config):
        self.config = config
        self.database_file = self.config['Data']['database_file']
        if not os.path.isfile(self.database_file):
            logging.error("Database file not found")
            return
        self.sql = b_sq.SQLconnector(self.database_file)
        self.main_loop()

    def main_loop(self):
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            if event is None or event == 'exit':
                    break
            if event is 'OK':
                print("VERIFIER LES INFORMATIONS ET INSERER LE LIVRE")
                ref0 = self.main_window.Element("inp_ref").Get()
                title0 = self.main_window.Element("inp_tit").Get()
                isbn0 = self.main_window.Element("inp_isbn").Get()
                author0 = self.main_window.Element("inp_aut").Get()
                editor0 = self.main_window.Element("inp_edi").Get()
                abstract0 = self.main_window.Element("inp_abs").Get()
                keywords0 = self.main_window.Element("inp_key").Get()
                if ref0 and title0 and author0 and editor0:
                    print("essayons d'introduire ce livre dans la bibliothèque")
                    r = self.sql.list_books(ref=ref0)
                    if r:
                        PopupErreur("Un livre avec la même REF existe déjà",
                                title="Nouveau livre")
                    #TODO: check REF format (AA0000)
                    else:
                        A = author0.split(":")
                        aut_id = int(A[0].strip())
                        A = editor0.split(":")
                        edi_id = int(A[0].strip())
                        print("REF: " + ref0)
                        print("ID author: " + str(aut_id))
                        print("ID editor: " + str(edi_id))
                        fields = dict()
                        fields['REF'] = ref0
                        fields['TITLE'] = title0.replace("'","''")
                        fields['ISBN'] = isbn0
                        fields['ID_AUTHOR'] = aut_id
                        fields['ID_EDITOR'] = edi_id
                        fields['ABSTRACT'] = abstract0.replace("'","''")
                        fields['KEYWORDS'] = keywords0.replace("'","''")
                        self.sql.ins_book(fields)
                        break
                elif not ref0:
                    PopupErreur("Le champ REF est obligatoire",
                                title="Nouveau livre")
                elif not title0:
                    PopupErreur("Le champ TITRE est obligatoire",
                                title="Nouveau livre")
                elif not author0:
                    PopupErreur("Le champ AUTEUR est obligatoire",
                                title="Nouveau livre")
                else:
                    PopupErreur("Le champ EDITEUR est obligatoire",
                                title="Nouveau livre")
            if event == 'inp_ref':
                print("REF suggestions")
            if event == 'aut_search':
                print("open author form")
                U = b_as.AuthorSearch(self.config,True)
                if U.current_author:
                    print("Auteur:", U.current_author)
                    self.main_window\
                        .Element("inp_aut")\
                        .Update(str(U.current_author[0]) + ": " + U.current_author[1] + ", " + U.current_author[2])
            if event == 'edi_search':
                print("open editor form")
                U = b_es.EditorSearch(self.config,True)
                if U.current_editor:
                    print("Editeur:", U.current_editor)
                    self.main_window\
                        .Element("inp_edi")\
                        .Update(str(U.current_editor[0]) + ": " + U.current_editor[1])
            print(event, values)
        self.main_window.Close()

    def make_main_window(self):
        #SAME DESIGN AS Catalog.book_window
        print("make main window")
        #Buttons
        bt_author_search = sg.Button(key="aut_search", button_text="chercher auteur")
        bt_editor_search = sg.Button(key="edi_search", button_text="chercher editeur")
        bt_ok = sg.Button(key="OK", button_text="CREER LIVRE")
        #bt_undo = sg.Button(key="undo", button_text="Défaire modifications")
        bt_exit = sg.Button(key="exit", button_text="ANNULER")
        #bt_up = sg.Button(key="up", button_text="Précédent")
        #bt_down = sg.Button(key="down", button_text="Suivant")

        #Text Elements
        tx = sg.Text("NOUVEAU LIVRE ",
                     key="tx_heading",
                     size=(25, 1),
                     font=big_font,
                     background_color=skyblue)
        #Inputtext Elements
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        input_ref = sg.InputText('', size=(10, 1),
                                 disabled=False,
                                 key="inp_ref", enable_events=True, do_not_clear=True)
        input_tit = sg.InputText('', size=(60, 1),
                                 disabled=False,
                                 key="inp_tit", enable_events=True, do_not_clear=True)
        input_isbn = sg.InputText('', size=(15, 1),
                                  disabled=False,
                                  key="inp_isbn", enable_events=True, do_not_clear=True)
        #input_las = sg.InputText(current_book[4], size=(30, 1),
        #                         key="inp_las", enable_events=True, do_not_clear=True)
        #input_fir = sg.InputText(current_book[5], size=(30, 1),
        #                         key="inp_fir", enable_events=True, do_not_clear=True)
        input_aut = sg.InputText('', size=(40, 1),
                                 disabled=True, key="inp_aut", 
                                 enable_events=True, do_not_clear=True)
        input_edi = sg.InputText('', size=(40, 1),
                                 disabled=True, key="inp_edi",
                                 enable_events=True, do_not_clear=True)         
        input_abs = sg.Multiline('', size=(60, 10),
                                 disabled=False,
                                 key="inp_abs", enable_events=True, do_not_clear=True)
        input_key = sg.InputText('', size=(60, 1),
                                 disabled=False,
                                 key="inp_key", enable_events=True, do_not_clear=True)
        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        #List Elements
        #r_authors = self.sql.list_authors()
        #author_list = [p[1] + ", " + p[2] + " ("+str(p[0])+")" for p in r_authors]
        #author_list.sort()
        #list_aut = sg.Combo(author_list,
        #                    size=(40, 1), key="lst_aut",enable_events=True)
        #r_editors = self.sql.list_editors()
        #editor_list = [p[1] + " ("+str(p[0])+")" for p in r_editors]
        #editor_list.sort()
        #list_edi = sg.Combo(editor_list,
        #                    size=(40, 1), key="lst_edi",enable_events=True)

        #Text Elements
        tx_ref = sg.Text('REF', key="tx_ref", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #REF
        tx_tit = sg.Text('TITRE', key="tx_tit", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #TITLE
        tx_isbn = sg.Text('ISBN', key="tx_isbn", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #ISBN
        #tx_las = sg.Text('AUTEUR (NOM)', size=(20, 1), font=medium_font,
        #                 background_color=skyblue)  #LASTNAME
        #tx_fir = sg.Text('AUTEUR (PRENOM)', size=(20, 1), font=medium_font,
        #                 background_color=skyblue)  #FIRSTNAME
        tx_aut = sg.Text('AUTEUR (ID: PRENOM, NOM)', key="tx_aut", size=(30, 1), font=medium_font,
                          background_color=skyblue)  #AUTHOR
        tx_aut1 = sg.Text('', size=(30, 1), font=medium_font,
                          background_color=skyblue)  #AUTHOR (EMPTY TEXT)
        tx_edi = sg.Text('EDITEUR (ID: NOM)', key="tx_edi", size=(30, 1), font=medium_font,
                         background_color=skyblue)  #EDITOR
        tx_edi1 = sg.Text('', size=(30, 1), font=medium_font,
                          background_color=skyblue)  #AUTHOR (EMPTY TEXT)
        tx_abs = sg.Text('RESUME', key="tx_abs", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #ABSTRACT
        tx_key = sg.Text('MOTS CLE', key="tx_key", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #KEYWORDS
        tx_sta = sg.Text('ETAT', key="tx_sta", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #STATE

        col = [[tx, ],
               [im_top2],
               [tx_ref, input_ref,
                tx_isbn, input_isbn],
               [tx_tit, input_tit],
               #[tx_las, input_las],
               #[tx_fir, input_fir],
               [tx_aut, input_aut],
               [tx_aut1, bt_author_search],
               [tx_edi, input_edi],
               [tx_edi1, bt_editor_search],
               [tx_key, input_key],
               [tx_abs, input_abs],
               [im_top2],
               [bt_ok, bt_exit]]
        
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.main_window = sg.Window("Fiche de livre", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
 

    def update_window(self):
        print("update window")


if __name__ == '__main__':
    import sys
    #change curdir to parent folder of catalog.py:
    #print("Maintenant je suis à:", os.getcwd())
    module_path , module_file = os.path.split(__file__)
    parent_path = os.path.join(module_path,'..')
    os.chdir(parent_path)

    #print("Maintenant je suis à:", os.getcwd())

    logging.basicConfig(filename="bibou.log", level=logging.INFO,
                        format='%(asctime)s %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')

    config = configparser.ConfigParser()
    if not os.path.isfile('settings.cfg'):
        print("Settings file not found")
        sys.exit()
    print("On a trouvé le fichier settings")
    config.read('settings.cfg')
    NewBook(config)
else:
    pass
