#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bibou editor search: graphic editor interface to search/insert/modify editors in databse
Created on Mon Nov 22 

@editor: redondo
"""

if __package__ is None or __package__ == "":
    import SQLconnector as b_sq
    from common import *
else:
    from . import SQLconnector as b_sq
    from .common import *

#TODO modify editors
#TODO export to excel
#TODO add column number of books per editor
class EditorSearch:
    def __init__(self, config, edition_mode=False):
        self.current_editor = []
        self.config = config
        self.edition_mode = edition_mode
        database_file = self.config['Data']['database_file']
        self.sql = b_sq.SQLconnector(database_file)
        self.main_loop()
        #if isinstance(self.selected_editor,int):
        #    self.current_editor = self.table_data[self.selected_editor]
    def main_loop(self):
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            #self.update_main_window()
            if event is None or event == 'exit':
                self.selected_editor = []
                self.current_editor = []
                break
            if event == 'select':
                self.selected_editor = values['main_table']
                if self.selected_editor:
                    self.selected_editor = self.selected_editor[0]
                    self.current_editor = self.table_data[self.selected_editor]
                    break
                PopupErreur("ERREUR: Il faut sélectionner un éditeur",title="",font=big_font)
            if event == 'erase':
                self.selected_editor = []
                self.current_editor = []
                self.main_window.Element("fil_name").Update('')
                self.filter_editor_list()
                self.update_main_window()
            if event == 'oldnew2':
                editor_name = self.main_window.Element("fil_name").Get()
                if editor_name:
                    answer = PopupOuiNon("Créer nouvel éditeur:\n" + editor_name,
                                         title="Nouvel éditeur")
                    if answer == "OUI":
                        self.sql.ins_editor(editor_name)
                else:
                    PopupErreur("Nom éditeur vide",
                                title="Nouvel éditeur")
                self.filter_editor_list()
                self.update_main_window()
            if event == 'new':
                    self.main_window.Hide()
                    self.new_editor_loop()
                    self.update_main_window()
                    self.main_window.UnHide()
            if event == 'modify':
                print("Let's change things!")
                self.selected_editor = values['main_table']
                if self.selected_editor:
                    self.selected_editor = self.selected_editor[0]
                    self.current_editor = self.table_data[self.selected_editor]
                    self.main_window.Hide()
                    self.editor_loop()
                    self.update_main_window()
                    self.main_window.UnHide()
                else:
                    PopupErreur("ERREUR: Il faut sélectionner un éditeur",title="",font=big_font)
            if event == 'main_table':
                self.selected_editor = values['main_table'][0] if values['main_table'] else []
                self.current_editor = self.table_data[self.selected_editor] if self.selected_editor else [[], [], []]
                #self.main_window.Element("fil_name").Update(self.current_editor[1])
            if event.startswith("fil"):
                print("filtrer")
                self.filter_editor_list()
                self.update_main_window()
            print(event, values)
        self.main_window.Close()
    def make_main_window(self):
        self.table_data = self.sql.list_editors_book_count()
        headers = ['ID', 'NOM', 'NB LIVRES']
        col_ws = [5, 20, 10]
        #TABLE
        TABLE = make_sg_table("main_table", self.table_data,
                              headers, col_ws, enable_events=True, num_rows=20)
        #buttons
        bt_select = sg.Button(key="select", button_text="SELECTIONNER",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_export = sg.Button(key="export", button_text="EXPORTER LISTE",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_erase = sg.Button(key="erase", button_text="EFFACER FORMULAIRE",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_exit = sg.Button(key="exit", button_text="SORTIR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_new = sg.Button(key="new", button_text="NOUVEL EDITEUR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_modify = sg.Button(key="modify", button_text="MODIFIER EDITEUR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        #input texts
        input_name = sg.InputText(key="fil_name", enable_events=True, do_not_clear=True)
        
        #Texts:
        tx = sg.Text("RECHERCHE D'EDITEURS (" + str(len(self.table_data)) + "):",
                     key="tx_heading",
                     size=(35, 1),
                     font=big_font,
                     background_color=skyblue)
        tx_name = sg.Text('NOM', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #title filter
        #MIDDLE COLUMN
        im_left,im_right,im_top,im_top2 = take_images()
        if self.edition_mode:
            col = [[im_top2],
                   [tx],
                   [TABLE],
                   [im_top2],
                   [tx_name, input_name],
                   [im_top2],
                   [bt_select, bt_modify, bt_new],
                   [bt_erase, bt_export, bt_exit]]
        else:
            col = [[im_top2],
                   [tx],
                   [TABLE],
                   [im_top2],
                   [tx_name, input_name],
                   [im_top2],
                   [bt_select, bt_erase, bt_exit]]
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.main_window = sg.Window("RECHERCHE D'EDITEUR", size=(1024, 768),
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     background_color=skyblue,
                                     margins=(0, 0)).Layout(layout)
    def update_main_window(self):
        TABLE = self.main_window.Element("main_table")
        TABLE.Update(values=self.table_data)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE.AlternatingRowColor = 'lightblue'
        TABLE.RowColor = 'blue'
        TX = self.main_window.Element("tx_heading")
        TX.Update(value="RECHERCHE D'EDITEURS (" + str(len(self.table_data)) + "):")
    def filter_editor_list(self):
        f_name = self.main_window.Element("fil_name").Get()
        #(ID,NAME)
        self.table_data = self.sql.list_editors_book_count(name=f_name)
    def editor_loop(self):
        self.make_editor_window()
        while True:
            event, values = self.editor_window.Read()
            if event is None or event == 'Exit' or event == 'retour':
                break
            if event == "update":
                self.change_editor()
                self.update_editor_window()
            if event == "undo":
                self.update_editor_window()
            if event.startswith("inp") or event == "update" or event == "undo":
                self.changed_editor_info()
            print(event, values)
        self.editor_window.Close()
    def make_editor_window(self):
        #'ID','NAME'
        current_editor = self.current_editor
        #Buttons
        bt_update = sg.Button(key="update", button_text="Mettre à jour")
        bt_undo = sg.Button(key="undo", button_text="Défaire modifications")
        bt_exit = sg.Button(key="retour", button_text="Retour")

        #Inputtext Elements
        #'ID','NAME'
        input_id = sg.InputText(current_editor[0], size=(30, 1),
                                disabled=True,
                                key="inp_id", enable_events=True, do_not_clear=True)
        input_name = sg.InputText(current_editor[1], size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_name", enable_events=True, do_not_clear=True)
        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        #List Elements
        #Text Elements
        tx_id = sg.Text('ID', key="tx_id", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #ID
        tx_name = sg.Text('NOM', key="tx_name", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #NAME
        if self.edition_mode:
            col = [[im_top2],
                   [tx_id, input_id],
                   [tx_name, input_name],
                   [im_top2],
                   [bt_update, bt_undo, bt_exit]]
        else:
            col = [[im_top2],
                   [tx_id, input_id],
                   [tx_name, input_name],
                   [im_top2],
                   [bt_exit]]
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.editor_window = sg.Window("Fiche d'éditeur", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
        self.update_editor_window()
    def update_editor_window(self):
        #'ID','NAME'
        self.current_editor = self.table_data[self.selected_editor]
        current_editor = self.current_editor
        self.editor_window.Element("inp_id").Update(current_editor[0])
        self.editor_window.Element("inp_name").Update(current_editor[1])
    def changed_editor_info(self):
        '''
            Detect any change in editor form and mark them in red color
        '''
        #'ID','NAME'
        current_editor = self.current_editor
        print(current_editor)
        #ID
        id_editor = int(self.editor_window.Element("inp_id").Get())
        if id_editor != current_editor[0]:
            tx_color='red'
        else:
            tx_color='black'
        self.editor_window.Element("tx_id").Update(text_color=tx_color)
        #NAME
        name = self.editor_window.Element("inp_name").Get()
        if name != current_editor[1]:
            tx_color='red'
        else:
            tx_color='black'
        self.editor_window.Element("tx_name").Update(text_color=tx_color)
    def change_editor(self):
        '''
            Take values in form and update book info in database
        '''
        print("Let's change things!")
        #'ID','NAME'
        id_editor = int(self.editor_window.Element("inp_id").Get())
        name = self.editor_window.Element("inp_name").Get()
        print("ID is " + str(id_editor))
        print("NAME is " + name)
        #'ID','NAME'
        current_editor = self.current_editor
        if id_editor != current_editor[0]:
            print("ERROR in id_editor")
            print("id_editor is " + id_editor + ", but current_editor[0] is " + str(current_editor[0]))
        if name != current_editor[1]:
            print("update NAME")
            self.sql.update_by_id(id_editor, "EDITOR", "NAME", name)
        self.table_data = self.sql.list_editors()
    def new_editor_loop(self):
        self.make_new_editor_window()
        while True:
            event, values = self.new_editor_window.Read()
            if event is None or event == 'Exit' or event == 'retour':
                break
            if event == "write":
                self.create_editor()
                break
            print(event, values)
        self.new_editor_window.Close()
    def make_new_editor_window(self):
        #'ID','NAME'
        #Buttons
        bt_write = sg.Button(key="write", button_text="Créer éditeur")
        bt_exit = sg.Button(key="retour", button_text="Retour")
        #Inputtext Elements
        #'ID','NAME'
        #input_id = sg.InputText(1000, size=(30, 1),
        #                        disabled=True,
        #                        key="inp_id", enable_events=True, do_not_clear=True)
        input_name = sg.InputText('', size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_name", enable_events=True, do_not_clear=True)
        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        #List Elements
        #Text Elements
        #tx_id = sg.Text('ID', key="tx_id", size=(10, 1), font=medium_font,
        #                 background_color=skyblue)  #ID
        tx_name = sg.Text('NOM', key="tx_name", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #NAME
        col = [[im_top2],
        #       [tx_id, input_id],
               [tx_name, input_name],
               [im_top2],
               [bt_write, bt_exit]]
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.new_editor_window = sg.Window("Création d'un nouvel éditeur", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
    def create_editor(self):
        '''
            Take values in form and insert new editor into editor table in database
        '''
        print("Let's create things!")
        #'ID','NAME'
        #id_editor = int(self.new_editor_window.Element("inp_id").Get())
        name = self.new_editor_window.Element("inp_name").Get()
        #print("ID is " + str(id_editor))
        print("NAME is " + name)
        #'ID','NAME'
        #TODO checkup other fields correctness
        if not name:
            PopupErreur("ERREUR: Le nom doit être renseigné",title="",font=big_font)
        else:
            answer = PopupOuiNon("Créer nouvel éditeur:\n"
                                 + name,
                                 title="Nouvel éditeur")
            if answer == "OUI":
                self.sql.ins_editor(name)
        self.table_data = self.sql.list_editors()

if __name__ == '__main__':
    import sys
    #change curdir to parent folder of catalog.py:
    #print("Maintenant je suis à:", os.getcwd())
    module_path , module_file = os.path.split(__file__)
    parent_path = os.path.join(module_path,'..')
    os.chdir(parent_path)

    #print("Maintenant je suis à:", os.getcwd())

    logging.basicConfig(filename="bibou.log", level=logging.INFO,
                        format='%(asctime)s %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    config = configparser.ConfigParser()
    if not os.path.isfile('settings.cfg'):
        print("Settings file not found")
        sys.exit()
    print("On a trouvé le fichier settings")
    config.read('settings.cfg')
    EditorSearch(config,True)
else:
    pass
