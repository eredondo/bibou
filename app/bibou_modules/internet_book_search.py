#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tools to query book info to intenet databases such as bnf.fr
Strongly inpired from PyGallica (https://github.com/ian-nai/PyGallica)
Created on 30 Sep 2020 

@author: redondo
"""

import requests
import xmltodict
import re

def bnf_isbn_search(isbn):
    baseUrl = 'http://catalogue.bnf.fr/api/SRU?version=1.2' +\
              '&operation=searchRetrieve' +\
              '&query=(bib.fuzzyISBN all %22'
    endUrl = '%22)&startRecord=1&recordSchema=dublincore'
    url = baseUrl + isbn + endUrl
    print(url)
    try:
        r = requests.get(url, stream=True)
    except requests.exceptions.ConnectionError as e:
        return -1
    XML = xmltodict.parse(r.content)
    n_records = XML['srw:searchRetrieveResponse']['srw:numberOfRecords']
    if int(n_records) == 0:
        print('rien à montrer')
        return None
    else:
        print('Found ' + n_records + ' books.')
    return XMLdict_to_book_list(XML)

def XMLdict_to_book_list(XML):
    """
        This function translates obtained XML into a list dict with three
        fields: title, author, editor. Each field is a list of strings.
    """
    n_records = XML['srw:searchRetrieveResponse']['srw:numberOfRecords']
    records = XML['srw:searchRetrieveResponse']['srw:records']['srw:record']
    if int(n_records) == 1:
        records = [records]
    books = list()
    for record in records:
        book = record['srw:recordData']['oai_dc:dc']
        title = book['dc:title']
        if 'dc:creator' in book.keys():
            authors = book['dc:creator']
        else:
            authors = 'Unkwonwn author'
        if 'dc:publisher' in book.keys():
            editors = book['dc:publisher']
        else:
            editors = 'Unkwonwn editor'
        if type(title)==type(str()):
            title = [title]
        if type(authors)==type(str()):
            authors = [authors]
        if type(editors)==type(str()):
            editors = [editors]
        authors = [clean_author_name(aut) for aut in authors]
        editors = [clean_editor_name(edi) for edi in editors]
        books.append({'title' : title,
                      'author' : authors,
                      'editor' : editors})
    return books

def clean_author_name(bnf_author):
    """
        bnf catalog returns authors as strings in the following form:
        'Lastname, Firstname (yyyy - yyyy). Auteur du texte'
        This function returns lastname and firstname
    """
    #first tentative: 'Lastname, Firstname ('
    author = re.search("^(?P<last>[A-Za-z'À-ÿ\- ]+), (?P<first>[A-Za-z'À-ÿ\- ]+) \(",bnf_author)
    if author:
        aut_dict = author.groupdict()
        return aut_dict['last'], aut_dict['first']
    #second tentative: 'Lastname ('
    author = re.search("^(?P<last>.+) \(",bnf_author)
    if author:
        aut_dict = author.groupdict()
        return aut_dict['last'], ''
    #if failed, return bfn_author as it is (lastname) and empty firstname
    return bnf_author, ''

def clean_editor_name(bnf_editor):
    """
        bnf catalog returns editors as strings in the following form:
        'Editor (Town)'
        This function returns lastname and firstname
    """
    #first tentative: 'Name ('
    editor = re.search("^(?P<name>.+) \(",bnf_editor)
    if editor:
        edi_dict = editor.groupdict()
        return edi_dict['name']
    else:#if failed, return bnf_editor as it is
        return bnf_editor



