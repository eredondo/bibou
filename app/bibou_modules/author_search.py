#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bibou author search: graphic author interface to search/insert/modify authors in database
Created on Mon Nov 22 

@author: redondo
"""

if __package__ is None or __package__ == "":
    import SQLconnector as b_sq
    from common import *
else:
    from . import SQLconnector as b_sq
    from .common import *

#TODO modify authors
class AuthorSearch:
    def __init__(self, config, edition_mode=False):
        self.current_author = []
        self.config = config
        self.edition_mode = edition_mode
        database_file = self.config['Data']['database_file']
        self.sql = b_sq.SQLconnector(database_file)
        self.main_loop()
        #if isinstance(self.selected_author,int):
        #    self.current_author = self.table_data[self.selected_author]
    def main_loop(self):
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            #self.update_main_window()
            if event is None or event == 'exit':
                self.selected_author = []
                self.current_author = []
                break
            if event == 'select':
                self.selected_author = values['main_table']
                if self.selected_author:
                    self.selected_author = self.selected_author[0]
                    self.current_author = self.table_data[self.selected_author]
                    break
                PopupErreur("ERREUR: Il faut sélectionner un auteur",title="",font=big_font)
            if event == 'erase':
                self.selected_author = []
                self.current_author = []
                self.main_window.Element("fil_fir").Update('')
                self.main_window.Element("fil_las").Update('')
                self.filter_author_list()
                self.update_main_window()
            if event == 'oldnew2':
                author_first = self.main_window.Element("fil_fir").Get()
                author_last = self.main_window.Element("fil_las").Get()
                if author_first or author_last:
                    answer = PopupOuiNon("Créer nouvel auteur:\n"
                                         + author_first
                                         + " "
                                         + author_last,
                                         title="Nouvel auteur")
                    if answer == "OUI":
                        self.sql.ins_author(author_first, author_last)
                else:
                    PopupErreur("Nom auteur vide",
                                title="Nouvel auteur")
                self.filter_author_list()
                self.update_main_window()
            if event == 'new':
                    self.main_window.Hide()
                    self.new_author_loop()
                    self.update_main_window()
                    self.main_window.UnHide()
            if event == 'modify':
                print("Let's change things!")
                self.selected_author = values['main_table']
                if self.selected_author:
                    self.selected_author = self.selected_author[0]
                    self.current_author = self.table_data[self.selected_author]
                    self.main_window.Hide()
                    self.author_loop()
                    self.update_main_window()
                    self.main_window.UnHide()
                else:
                    PopupErreur("ERREUR: Il faut sélectionner un auteur",title="",font=big_font)
            if event == 'main_table':
                self.selected_author = values['main_table'][0] if values['main_table'] else []
                self.current_author = self.table_data[self.selected_author] if self.selected_author else [[], [], []]
                #self.main_window.Element("fil_fir").Update(self.current_author[1])
                #self.main_window.Element("fil_las").Update(self.current_author[2])
            if event.startswith("fil"):
                print("filtrer")
                self.filter_author_list()
                self.update_main_window()
            print(event, values)
        self.main_window.Close()
    def make_main_window(self):
        self.table_data = self.sql.list_authors_book_count()
        headers = ['ID', 'PRENOM', 'NOM', 'NB LIVRES']
        col_ws = [5, 20, 20, 10]
        #TABLE
        TABLE = make_sg_table("main_table", self.table_data,
                              headers, col_ws, enable_events=True, num_rows=20)
        #buttons
        bt_select = sg.Button(key="select", button_text="SELECTIONNER",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_export = sg.Button(key="export", button_text="EXPORTER LISTE",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_erase = sg.Button(key="erase", button_text="EFFACER FORMULAIRE",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_exit = sg.Button(key="exit", button_text="SORTIR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_new = sg.Button(key="new", button_text="NOUVEL AUTEUR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_modify = sg.Button(key="modify", button_text="MODIFIER AUTEUR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        #input texts
        input_fir = sg.InputText(key="fil_fir", enable_events=True, do_not_clear=True)
        input_las = sg.InputText(key="fil_las", enable_events=True, do_not_clear=True)
        
        #Texts:
        tx = sg.Text("RECHERCHE D'AUTEURS (" + str(len(self.table_data)) + "):",
                     key="tx_heading",
                     size=(35, 1),
                     font=big_font,
                     background_color=skyblue)
        tx_fir = sg.Text('PRENOM', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #title filter
        tx_las = sg.Text('NOM', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #lastname filter
        #MIDDLE COLUMN
        im_left,im_right,im_top,im_top2 = take_images()
        if self.edition_mode:
            col = [[im_top2],
                   [tx],
                   [TABLE],
                   [im_top2],
                   [tx_fir, input_fir],
                   [tx_las, input_las],
                   [im_top2],
                   [bt_select, bt_modify, bt_new],
                   [bt_erase, bt_export, bt_exit]]
        else:
            col = [[im_top2],
                   [tx],
                   [TABLE],
                   [im_top2],
                   [tx_fir, input_fir],
                   [tx_las, input_las],
                   [im_top2],
                   [bt_select, bt_erase, bt_exit]]
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.main_window = sg.Window("RECHERCHE D'AUTEUR", size=(1024, 768),
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     background_color=skyblue,
                                     margins=(0, 0)).Layout(layout)
    def update_main_window(self):
        TABLE = self.main_window.Element("main_table")
        TABLE.Update(values=self.table_data)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE.AlternatingRowColor = 'lightblue'
        TABLE.RowColor = 'blue'
        TX = self.main_window.Element("tx_heading")
        TX.Update(value="RECHERCHE D'AUTEURS (" + str(len(self.table_data)) + "):")
    def filter_author_list(self):
        f_firstname = self.main_window.Element("fil_fir").Get()
        f_lastname = self.main_window.Element("fil_las").Get()
        #(ID,FIRSTNAME,LASTNAME)
        self.table_data = self.sql.list_authors_book_count(firstname=f_firstname,
                                lastname=f_lastname)
    def author_loop(self):
        self.make_author_window()
        while True:
            event, values = self.author_window.Read()
            if event is None or event == 'Exit' or event == 'retour':
                break
            if event == "update":
                self.change_author()
                self.update_author_window()
            if event == "undo":
                self.update_author_window()
            if event.startswith("inp") or event == "update" or event == "undo":
                self.changed_author_info()
            print(event, values)
        self.author_window.Close()
    def make_author_window(self):
        #'ID','LASTNAME','FIRSTNAME'
        current_author = self.current_author
        #Buttons
        bt_update = sg.Button(key="update", button_text="Mettre à jour")
        bt_undo = sg.Button(key="undo", button_text="Défaire modifications")
        bt_exit = sg.Button(key="retour", button_text="Retour")

        #Inputtext Elements
        #'ID','LASTNAME','FIRSTNAME'
        input_id = sg.InputText(current_author[0], size=(30, 1),
                                disabled=True,
                                key="inp_id", enable_events=True, do_not_clear=True)
        input_las = sg.InputText(current_author[1], size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_las", enable_events=True, do_not_clear=True)
        input_fir = sg.InputText(current_author[2], size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_fir", enable_events=True, do_not_clear=True)
        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        #List Elements
        #Text Elements
        tx_id = sg.Text('ID', key="tx_id", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #ID
        tx_las = sg.Text('NOM', key="tx_las", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #LASTNAME
        tx_fir = sg.Text('PRENOM', key="tx_fir", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #FIRSTNAME
        if self.edition_mode:
            col = [[im_top2],
                   [tx_id, input_id],
                   [tx_las, input_las],
                   [tx_fir, input_fir],
                   [im_top2],
                   [bt_update, bt_undo, bt_exit]]
        else:
            col = [[im_top2],
                   [tx_id, input_id],
                   [tx_las, input_las],
                   [tx_fir, input_fir],
                   [im_top2],
                   [bt_exit]]
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.author_window = sg.Window("Fiche d'auteur", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
        self.update_author_window()
    def update_author_window(self):
        #'ID','LASTNAME','FIRSTNAME'
        self.current_author = self.table_data[self.selected_author]
        current_author = self.current_author
        self.author_window.Element("inp_id").Update(current_author[0])
        self.author_window.Element("inp_fir").Update(current_author[1])
        self.author_window.Element("inp_las").Update(current_author[2])
    def changed_author_info(self):
        '''
            Detect any change in author form and mark them in red color
        '''
        #'ID','LASTNAME','FIRSTNAME'
        current_author = self.current_author
        print(current_author)
        #ID
        id_author = int(self.author_window.Element("inp_id").Get())
        if id_author != current_author[0]:
            tx_color='red'
        else:
            tx_color='black'
        self.author_window.Element("tx_id").Update(text_color=tx_color)
        #FIRSTNAME
        firstname = self.author_window.Element("inp_fir").Get()
        if firstname != current_author[1]:
            tx_color='red'
        else:
            tx_color='black'
        self.author_window.Element("tx_fir").Update(text_color=tx_color)
        #LASTNAME
        lastname = self.author_window.Element("inp_las").Get()
        if lastname != current_author[2]:
            tx_color='red'
            print(lastname + " is different than " + current_author[1])
        else:
            tx_color='black'
        self.author_window.Element("tx_las").Update(text_color=tx_color)
    def change_author(self):
        '''
            Take values in form and update book info in database
        '''
        print("Let's change things!")
        #'ID','LASTNAME','FIRSTNAME'
        id_author = int(self.author_window.Element("inp_id").Get())
        firstname = self.author_window.Element("inp_fir").Get()
        lastname = self.author_window.Element("inp_las").Get()
        print("ID is " + str(id_author))
        print("FIRSTNAME is " + firstname)
        print("LASTNAME is " + lastname)
        #'ID','LASTNAME','FIRSTNAME'
        current_author = self.current_author
        if id_author != current_author[0]:
            print("ERROR in id_author")
            print("id_author is " + id_author + ", but current_author[0] is " + str(current_author[0]))
        if firstname != current_author[1]:
            print("update FIRSTNAME")
            self.sql.update_by_id(id_author, "AUTHOR", "FIRSTNAME", firstname)
        if lastname != current_author[2]:
            print("update LASTNAME")
            self.sql.update_by_id(id_author, "AUTHOR", "LASTNAME", lastname)
        self.table_data = self.sql.list_authors()
    def new_author_loop(self):
        self.make_new_author_window()
        while True:
            event, values = self.new_author_window.Read()
            if event is None or event == 'Exit' or event == 'retour':
                break
            if event == "write":
                self.create_author()
                break
            print(event, values)
        self.new_author_window.Close()
    def make_new_author_window(self):
        #'ID','LASTNAME','FIRSTNAME'
        #Buttons
        bt_write = sg.Button(key="write", button_text="Créer auteur")
        bt_exit = sg.Button(key="retour", button_text="Retour")
        #Inputtext Elements
        #'ID','LASTNAME','FIRSTNAME'
        #input_id = sg.InputText(1000, size=(30, 1),
        #                        disabled=True,
        #                        key="inp_id", enable_events=True, do_not_clear=True)
        input_las = sg.InputText('', size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_las", enable_events=True, do_not_clear=True)
        input_fir = sg.InputText('', size=(30, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_fir", enable_events=True, do_not_clear=True)
        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        #List Elements
        #Text Elements
        #tx_id = sg.Text('ID', key="tx_id", size=(10, 1), font=medium_font,
        #                 background_color=skyblue)  #ID
        tx_las = sg.Text('NOM', key="tx_las", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #LASTNAME
        tx_fir = sg.Text('PRENOM', key="tx_fir", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #FIRSTNAME
        col = [[im_top2],
        #       [tx_id, input_id],
               [tx_las, input_las],
               [tx_fir, input_fir],
               [im_top2],
               [bt_write, bt_exit]]
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.new_author_window = sg.Window("Création d'un nouvel auteur", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
    def create_author(self):
        '''
            Take values in form and insert new author into author table in database
        '''
        print("Let's create things!")
        #'ID','LASTNAME','FIRSTNAME'
        #id_author = int(self.new_author_window.Element("inp_id").Get())
        firstname = self.new_author_window.Element("inp_fir").Get()
        lastname = self.new_author_window.Element("inp_las").Get()
        #print("ID is " + str(id_author))
        print("FIRSTNAME is " + firstname)
        print("LASTNAME is " + lastname)
        #'ID','LASTNAME','FIRSTNAME'
        #TODO checkup other fields correctness
        if not firstname:
            PopupErreur("ERREUR: Le prénom doit être renseigné",title="",font=big_font)
        elif not lastname:
            PopupErreur("ERREUR: Le nom doit être renseigné",title="",font=big_font)
        else:
            answer = PopupOuiNon("Créer nouvel auteur:\n"
                                 + firstname
                                 + " "
                                 + lastname,
                                 title="Nouvel auteur")
            if answer == "OUI":
                self.sql.ins_author(firstname, lastname)
        self.table_data = self.sql.list_authors()

if __name__ == '__main__':
    import sys
    #change curdir to parent folder of catalog.py:
    #print("Maintenant je suis à:", os.getcwd())
    module_path , module_file = os.path.split(__file__)
    parent_path = os.path.join(module_path,'..')
    os.chdir(parent_path)

    #print("Maintenant je suis à:", os.getcwd())

    logging.basicConfig(filename="bibou.log", level=logging.INFO,
                        format='%(asctime)s %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    config = configparser.ConfigParser()
    if not os.path.isfile('settings.cfg'):
        print("Settings file not found")
        sys.exit()
    print("On a trouvé le fichier settings")
    config.read('settings.cfg')
    AuthorSearch(config,True)
else:
    pass
