#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bibou catalog: graphic user interface for viewing and editing books in the library
Created on Mon Sep 30 16:36:21 2019

@author: redondo
"""

if __package__ is None or __package__ == "":
    import SQLconnector as b_sq
    from common import *
    import new_book as b_nb
    import new_book_isbn as b_nbi

else:
    from . import SQLconnector as b_sq
    from .common import *
    from . import new_book as b_nb
    from . import new_book_isbn as b_nbi


#TODO problem in make book window, set selected item in lists (author, editor,state)
#FIXME after ordering, order table_data (ordering only working in visualisation 
# >>>>>>>>> the problem is between table_data and table_fulldata
#TODO change_book: update editor and author id
#TODO: use logging for errors and debug
#FIXME when returning to main window, update selected book based on navigation buttons of book window

class Catalog:
    '''
        Application based on PySimpleGUI to view and manage the library catalog
    '''
    def __init__(self,edition_mode):
        #edition_mode:
        #True = book form can edit fields
        #False = book form in read only mode
        self.edition_mode = edition_mode
        self.config = configparser.ConfigParser()
        if not os.path.isfile('settings.cfg'):
            logging.error("Settings file not found")
            return
        self.config.read('settings.cfg')
        self.database_file = self.config['Data']['database_file']
        if not os.path.isfile(self.database_file):
            logging.error("Database file not found")
            return
        #sql connector (a mettre plus haut?)
        self.sql = b_sq.SQLconnector(self.database_file)
        self.sort_index = -1
        self.sort_direction = 1    #see order_table method
        self.current_book_index = [] #see update_main_window
        self.main_loop()
    def main_loop(self):
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            #self.update_main_window()
            if event is None or event == 'Exit' or event == 'sortir':
                break
            if event == 'fiche':
                if values["main_table"]:
                    self.current_book_index = values["main_table"][0]
                else:
                    self.current_book_index = 0
                print("OUVRIR LA FICHE")
                self.main_window.Hide()
                self.book_loop()
                self.update_main_window()
                self.main_window.UnHide()
            if event == 'new':
                self.main_window.Hide()
                #print("New book")
                b_nb.NewBook(self.config)
                self.main_window.UnHide()
            if event == 'new_isbn':
                self.main_window.Hide()
                #print("New book")
                b_nbi.NewBookIsbn(self.config)
                self.main_window.UnHide()
            if event.startswith("tri"):
                print("FAIRE LE TRI")
#                self.retour_pret()
                self.order_table(event)
                self.current_book_index = [] #see update_main_window
                self.update_main_window()
            if event.startswith("fil"):
                print("FILTRER")
                self.filter_book_list()
                self.current_book_index = [] #see update_main_window
                self.update_main_window()
            if event == 'c':
                print("c")
#                self.prets_en_cours()
            print(event, values)
        self.main_window.Close()
    def book_loop(self):
        self.make_book_window()
        #self.update_book_window()
        while True:
            event, values = self.book_window.Read()
            if event is None or event == 'Exit' or event == 'retour':
                break
            if event == "up":
                if self.current_book_index > 0:
                    self.current_book_index = self.current_book_index-1
                else:
                    self.current_book_index = len(self.table_data)-1
                self.update_book_window()
            if event == "down":
                if self.current_book_index < len(self.table_data)-1:
                    self.current_book_index = self.current_book_index+1
                else:
                    self.current_book_index = 0
                self.update_book_window()
            if event == "update":
                self.change_book()
            if event == "undo":
                self.update_book_window()
            if event == "lst_sta":#changed book state in combo box
                new_sta = values['lst_sta']
                new_sta =  int(new_sta.split(" ")[0])
                #print("new value is: " + str(new_sta))
                self.book_window.Element("inp_sta").Update(new_sta)
            if event == "lst_aut":#changed book author in combo box
                #author in lst_aut is: "first, last (ID)"
                new_aut = values['lst_aut']
                #get ID from author:
                new_aut_id =  int(new_aut.split("(")[1][:-1])
                r_author = self.sql.get_author_by_id(new_aut_id)
                #TODO: try catch
                firstname,lastname = (r_author[1],r_author[2])
                self.book_window.Element("inp_aut").Update(firstname + ", " + lastname)
            if event == "lst_edi":#changed book editor in combo box
                #editor in lst_edi is: "name (ID)"
                new_edi = values['lst_edi']
                #get ID from editor:
                new_edi_id =  int(new_edi.split("(")[1][:-1])
                r_editor = self.sql.get_editor_by_id(new_edi_id)
                #TODO: try catch
                self.book_window.Element("inp_edi").Update(r_editor[1])
            if event.startswith("inp") or event.startswith("lst")\
               or event == "update" or event == "undo":
                self.changed_book_info()
            print(event, values)
        self.book_window.Close()
    def make_main_window(self):
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        r = self.sql.list_books()
        headers = ['ID', 'REF', 'TITLE', 'ISBN', 'LASTNAME', 'FIRSTNAME',
                   'EDITOR', 'ABSTRACT', 'KEYWORDS', 'STATE']
        col_ws = [5, 5, 40, 10, 10, 10, 15, 30, 20, 3]
        # TODO rearrange columns:
        #(REF,TITLE,LASTNAME,FIRSTNAME,EDITOR,KEYWORDS,STATE,ABSTRACT,ISBN,ID)
        #reducing and rearranging columns
        #'REF','TITLE','LASTNAME','FIRSTNAME','EDITOR','STATE'
        #r1 = [p[1:3]+p[4:7]+(p[-1],) for p in r]
        #self.table_data=r1
        self.format_book_list(r)
        headers = ['REF', 'TITRE', 'AUT. NOM', 'AUT. PRENOM', 'EDITEUR', 'ETAT']
        col_ws = [5, 15, 10, 10, 8, 5]
        #TABLE
        TABLE = make_sg_table("main_table", self.table_data,
                              headers, col_ws, num_rows=18)
        #book_list = ["|"+str(p[0])+"|"+"|".join(p[1:-1])+"|"+str(p[-1]) for p in r]
        #book_list = self.format_book_list(r)
        #lbox = sg.Listbox(key='list', values=book_list, size=(100, 10), font=medium_font)

        #Buttons
        bt_tri_ref = sg.Button(key="tri_0", button_text=headers[0],
                               font=medium_font, border_width=1,
                               size=(col_ws[0], 0), pad=(0, 0))
        bt_tri_tit = sg.Button(key="tri_1", button_text=headers[1],
                               font=medium_font, border_width=1,
                               size=(col_ws[1]+4, 0), pad=(0, 0))
        bt_tri_las = sg.Button(key="tri_2", button_text=headers[2],
                               font=medium_font, border_width=1,
                               size=(col_ws[2]+1, 0), pad=(0, 0))
        bt_tri_fir = sg.Button(key="tri_3", button_text=headers[3],
                               font=medium_font, border_width=1,
                               size=(col_ws[3]+1, 0), pad=(0, 0))
        bt_tri_edi = sg.Button(key="tri_4", button_text=headers[4],
                               font=medium_font, border_width=1,
                               size=(col_ws[4]+1, 0), pad=(0, 0))
        bt_tri_sta = sg.Button(key="tri_5", button_text=headers[5],
                               font=medium_font, border_width=1,
                               size=(col_ws[5], 0), pad=(0, 0))
        bt_book = sg.Button(key="fiche", button_text="Fiche du livre")
        bt_new = sg.Button(key="new", button_text="Nouveau livre")
        bt_exit = sg.Button(key="sortir", button_text="Sortir")
        bt_new_isbn = sg.Button(key="new_isbn",
                                    button_text="Nouveau livre (recherche ISBN)")
        #input texts
        input_ref = sg.InputText(key="fil_ref", enable_events=True, do_not_clear=True)
        input_tit = sg.InputText(key="fil_tit", enable_events=True, do_not_clear=True)
        input_las = sg.InputText(key="fil_las", enable_events=True, do_not_clear=True)
        input_fir = sg.InputText(key="fil_fir", enable_events=True, do_not_clear=True)
        input_key = sg.InputText(key="fil_key", enable_events=True, do_not_clear=True)
        input_abs = sg.InputText(key="fil_abs", enable_events=True, do_not_clear=True)

        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        # All the stuff inside your window
        tx = sg.Text("LISTE DE LIVRES (" + str(len(self.table_data)) + "):",
                     key="tx_heading",
                     size=(35, 1),
                     font=big_font,
                     background_color=skyblue)
        tx_blank = sg.Text("             ", font=medium_font,background_color=skyblue)
        ttri = sg.Text("TRIER:", font=medium_font,
                       background_color=skyblue)
        tx_ref = sg.Text('Ref.', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #REF filter
        tx_tit = sg.Text('Titre', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #title filter
        tx_las = sg.Text('Nom auteur', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #lastname filter
        tx_fir = sg.Text('Prénom auteur', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #firstname filter
        tx_key = sg.Text('Mots clé', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #keywords filter
        tx_abs = sg.Text('Résumé', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #abstract filter
        
        if self.edition_mode:
            col = [[tx],
                   [im_top2],
                   [TABLE],
                   [bt_tri_ref, bt_tri_tit, bt_tri_las, bt_tri_fir, bt_tri_edi, bt_tri_sta],
                   [im_top2],
                   [tx_ref, input_ref],
                   [tx_tit, input_tit],
                   [tx_las, input_las],
                   [tx_fir, input_fir],
                   [tx_key, input_key],
                   [tx_abs, input_abs],
                   [im_top2],
                   [bt_book, bt_new, bt_new_isbn, bt_exit]]
        else:
            col = [[tx],
                   [im_top2],
                   [TABLE],
                   [bt_tri_ref, bt_tri_tit, bt_tri_las, bt_tri_fir, bt_tri_edi, bt_tri_sta],
                   [im_top2],
                   [tx_ref, input_ref],
                   [tx_tit, input_tit],
                   [tx_las, input_las],
                   [tx_fir, input_fir],
                   [tx_key, input_key],
                   [tx_abs, input_abs],
                   [im_top2],
                   [bt_book, bt_exit]]
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.main_window = sg.Window("Catalogue", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout)
    def make_book_window(self):
        #'REF','TITLE','LASTNAME','FIRSTNAME','EDITOR','STATE'
        current_book = self.table_fulldata[self.current_book_index]
        #Buttons
        bt_update = sg.Button(key="update", button_text="Mettre à jour")
        bt_undo = sg.Button(key="undo", button_text="Défaire modifications")
        bt_exit = sg.Button(key="retour", button_text="Retour")
        bt_up = sg.Button(key="up", button_text="Précédent")
        bt_down = sg.Button(key="down", button_text="Suivant")

        #Text Elements
        tx = sg.Text("FICHE NUMÉRO " \
                     + str(self.current_book_index+1) \
                     + " / " + str(len(self.table_data)),
                     key="tx_heading",
                     size=(25, 1),
                     font=big_font,
                     background_color=skyblue)
        #Inputtext Elements
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        input_id = sg.InputText(current_book[0], size=(5, 1),
                                disabled=True,
                                key="inp_id", enable_events=True, do_not_clear=True)
        input_ref = sg.InputText(current_book[1], size=(10, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_ref", enable_events=True, do_not_clear=True)
        input_tit = sg.InputText(current_book[2], size=(60, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_tit", enable_events=True, do_not_clear=True)
        input_isbn = sg.InputText(current_book[3], size=(15, 1),
                                  disabled=not self.edition_mode,
                                  key="inp_isbn", enable_events=True, do_not_clear=True)
        #input_las = sg.InputText(current_book[4], size=(30, 1),
        #                         key="inp_las", enable_events=True, do_not_clear=True)
        #input_fir = sg.InputText(current_book[5], size=(30, 1),
        #                         key="inp_fir", enable_events=True, do_not_clear=True)
        input_aut = sg.InputText(current_book[5]+ ", " + current_book[4], size=(40, 1),
                                 disabled=True, key="inp_aut", 
                                 enable_events=True, do_not_clear=True)
        input_edi = sg.InputText(current_book[6], size=(40, 1),
                                 disabled=True, key="inp_edi",
                                 enable_events=True, do_not_clear=True)         
        input_abs = sg.Multiline(current_book[7], size=(60, 10),
                                 disabled=not self.edition_mode,
                                 key="inp_abs", enable_events=True, do_not_clear=True)
        input_key = sg.InputText(current_book[8], size=(60, 1),
                                 disabled=not self.edition_mode,
                                 key="inp_key", enable_events=True, do_not_clear=True)
        input_sta = sg.InputText(current_book[9], size=(5, 1),
                                 disabled=True,
                                 key="inp_sta", enable_events=True,
                                 do_not_clear=True)                                
        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        #List Elements
        list_sta = sg.Combo(["1 (en prêt)", "0 (disponible)",
                             "-1 (en salle uniq.)", "-2 (non disponible)"],
                            default_value="0 (disponible)",
                            size=(15, 1), key="lst_sta",enable_events=True)
        r_authors = self.sql.list_authors()
        author_list = [p[1] + ", " + p[2] + " ("+str(p[0])+")" for p in r_authors]
        author_list.sort()
        list_aut = sg.Combo(author_list,
                            size=(40, 1), key="lst_aut",enable_events=True)
        r_editors = self.sql.list_editors()
        editor_list = [p[1] + " ("+str(p[0])+")" for p in r_editors]
        editor_list.sort()
        list_edi = sg.Combo(editor_list,
                            size=(40, 1), key="lst_edi",enable_events=True)

        #Text Elements
        tx_id = sg.Text('ID', key="tx_id", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #ID
        tx_ref = sg.Text('REF', key="tx_ref", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #REF
        tx_tit = sg.Text('TITRE', key="tx_tit", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #TITLE
        tx_isbn = sg.Text('ISBN', key="tx_isbn", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #ISBN
        #tx_las = sg.Text('AUTEUR (NOM)', size=(20, 1), font=medium_font,
        #                 background_color=skyblue)  #LASTNAME
        #tx_fir = sg.Text('AUTEUR (PRENOM)', size=(20, 1), font=medium_font,
        #                 background_color=skyblue)  #FIRSTNAME
        tx_aut = sg.Text('AUTEUR (PRENOM, NOM)', key="tx_aut", size=(30, 1), font=medium_font,
                          background_color=skyblue)  #AUTHOR
        tx_aut1 = sg.Text('', size=(30, 1), font=medium_font,
                          background_color=skyblue)  #AUTHOR (EMPTY TEXT)
        tx_edi = sg.Text('EDITEUR', key="tx_edi", size=(30, 1), font=medium_font,
                         background_color=skyblue)  #EDITOR
        tx_edi1 = sg.Text('', size=(30, 1), font=medium_font,
                          background_color=skyblue)  #AUTHOR (EMPTY TEXT)
        tx_abs = sg.Text('RESUME', key="tx_abs", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #ABSTRACT
        tx_key = sg.Text('MOTS CLE', key="tx_key", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #KEYWORDS
        tx_sta = sg.Text('ETAT', key="tx_sta", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #STATE

        if self.edition_mode:
            col = [[tx, ],
                   [im_top2],
                   [tx_id, input_id, tx_ref, input_ref,
                    tx_isbn, input_isbn],
                   [tx_tit, input_tit],
                   #[tx_las, input_las],
                   #[tx_fir, input_fir],
                   [tx_aut, input_aut],
                   [tx_aut1, list_aut],
                   [tx_edi, input_edi],
                   [tx_edi1, list_edi],
                   [tx_key, input_key],
                   [tx_abs, input_abs],
                   [tx_sta, input_sta, list_sta],
                   [im_top2],
                   [bt_up, bt_down, bt_update, bt_undo, bt_exit]]
        else:
            col = [[tx, ],
                   [tx_ref, input_ref],
                   [tx_tit, input_tit],
                   #[tx_las, input_las],
                   #[tx_fir, input_fir],
                   [tx_aut, input_aut],
                   [tx_edi, input_edi],
                   [tx_key, input_key],
                   [tx_abs, input_abs],
                   [tx_sta, input_sta],
                   [im_top2],
                   [bt_up, bt_down, bt_exit]]
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.book_window = sg.Window("Fiche de livre", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
        self.update_book_window()

    def update_main_window(self):
        TABLE = self.main_window.Element("main_table")
        TABLE.Update(values=self.table_data)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE.AlternatingRowColor = 'lightblue'
        TABLE.RowColor = 'blue'
        TX = self.main_window.Element("tx_heading")
        TX.Update(value="LISTE DE LIVRES (" + str(len(self.table_data)) + "):")
    def update_book_window(self):
        #'ID','REF','TITLE','ISBN','LASTNAME','FIRSTNAME',
        #'EDITOR','ABSTRACT','KEYWORDS','STATE'
        current_book = self.table_fulldata[self.current_book_index]
        self.book_window.Element("inp_ref").Update(current_book[1])
        self.book_window.Element("inp_tit").Update(current_book[2])
        #self.book_window.Element("inp_las").Update(current_book[4])
        #self.book_window.Element("inp_fir").Update(current_book[5])
        self.book_window.Element("inp_aut").Update(current_book[5] + ", " + current_book[4])
        self.book_window.Element("inp_edi").Update(current_book[6])
        self.book_window.Element("inp_abs").Update(current_book[7])
        self.book_window.Element("inp_key").Update(current_book[8])
        self.book_window.Element("inp_sta").Update(current_book[9])
        if self.edition_mode:
            self.book_window.Element("inp_id").Update(current_book[0])
            self.book_window.Element("inp_isbn").Update(current_book[3])
            #update lists (state,author,editor)
            #state
            self.book_window.Element("lst_sta").Update(set_to_index=-current_book[9]+1)
            #author
            r_author = self.sql.list_authors(current_book[5], current_book[4])
            self.book_window.Element("lst_aut")\
            .Update(value=r_author[0][1] + ", " + r_author[0][2]+ " (" + str(r_author[0][0])+ ")")
            #editor
            r_editor = self.sql.list_editors(current_book[6])
            self.book_window.Element("lst_edi")\
            .Update(value=r_editor[0][1]+ " (" + str(r_editor[0][0])+ ")")
        #update book number
        self.book_window.Element("tx_heading")\
        .Update("FICHE NUMÉRO " \
                + str(self.current_book_index+1) \
                + " / " + str(len(self.table_data)))
    def changed_book_info(self):
        '''
            Detect any change in book form and mark them in red color
        '''
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        current_book = self.table_fulldata[self.current_book_index]
        #ID
        id_book = int(self.book_window.Element("inp_id").Get())
        if id_book != current_book[0]:
            tx_color='red'
        else:
            tx_color='black'
        self.book_window.Element("tx_id").Update(text_color=tx_color)
        #REF
        ref_book = self.book_window.Element("inp_ref").Get()
        if ref_book != current_book[1]:
            tx_color='red'
        else:
            tx_color='black'
        self.book_window.Element("tx_ref").Update(text_color=tx_color)
        #TITLE
        title_book = self.book_window.Element("inp_tit").Get()
        if title_book != current_book[2]:
            tx_color='red'
        else:
            tx_color='black'
        self.book_window.Element("tx_tit").Update(text_color=tx_color)
        #ISBN
        isbn_book = self.book_window.Element("inp_isbn").Get()
        if isbn_book != current_book[3]:
            tx_color='red'
        else:
            tx_color='black'
        self.book_window.Element("tx_isbn").Update(text_color=tx_color)
        #AUTHOR 
        authorname_book = self.book_window.Element("inp_aut").Get()
        if authorname_book != current_book[5] + ", " + current_book[4]:
            tx_color='red'
        else:
            tx_color='black'
        self.book_window.Element("tx_aut").Update(text_color=tx_color)
        #EDITOR
        editor_book = self.book_window.Element("inp_edi").Get()
        if editor_book != current_book[6]:
            tx_color='red'
        else:
            tx_color='black'
        self.book_window.Element("tx_edi").Update(text_color=tx_color)
        #ABSTRACT
        abstract_book = self.book_window.Element("inp_abs").Get()
        if abstract_book[:-1] != current_book[7]:
            tx_color='red'
        else:
            tx_color='black'
        self.book_window.Element("tx_abs").Update(text_color=tx_color)
        #KEYWORDS
        keywords_book = self.book_window.Element("inp_key").Get()
        if keywords_book != current_book[8]:
            tx_color='red'
        else:
            tx_color='black'
        self.book_window.Element("tx_key").Update(text_color=tx_color)
        #STATE
        state_book = int(self.book_window.Element("inp_sta").Get())
        if state_book != current_book[9]:
            tx_color='red'
        else:
            tx_color='black'
        self.book_window.Element("tx_sta").Update(text_color=tx_color)
        
    def change_book(self):
        '''
            Take values in form and update book info in database
        '''
        print("Let's change things!")
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        id_book = int(self.book_window.Element("inp_id").Get())
        ref_book = self.book_window.Element("inp_ref").Get()
        title_book = self.book_window.Element("inp_tit").Get()
        isbn_book = self.book_window.Element("inp_isbn").Get()
        authorname_book = self.book_window.Element("inp_aut").Get()
        editor_book = self.book_window.Element("inp_edi").Get()
        abstract_book = self.book_window.Element("inp_abs").Get()
        keywords_book = self.book_window.Element("inp_key").Get()
        state_book = int(self.book_window.Element("inp_sta").Get())
        print("ID is " + str(id_book))
        print("REF is " + ref_book)
        print("TITLE is " + title_book)
        print("ISBN is " + isbn_book)
        print("AUTHORNAME is " + authorname_book)
        print("EDITOR is " + editor_book)
        print("ABSTRACT is " + abstract_book)
        print("STATE is " + str(state_book))
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        current_book = self.table_fulldata[self.current_book_index]
        if id_book != current_book[0]:
            print("ERROR in id_book")
            print("id_book is " + id_book + ", but current_book[0] is " + str(current_book[0]))
        if ref_book != current_book[1]:
            print("update REF")
            self.sql.update_by_id(id_book, "BOOK", "REF", ref_book)
        if title_book != current_book[2]:
            print("update TITLE")
            self.sql.update_by_id(id_book, "BOOK", "TITLE", title_book)
        if isbn_book != current_book[3]:
            print("update ISBN")
            self.sql.update_by_id(id_book, "BOOK", "ISBN", isbn_book)
        if authorname_book != current_book[5] + ", " + current_book[4] :
            print("update AUTHOR")
        if editor_book != current_book[6]:
            print("update EDITOR")
        if abstract_book[:-1] != current_book[7]:
            #multiline input text added extra line feed (ASCCI=10) at the end
            print("update ABSTRACT")
            self.sql.update_by_id(id_book, "BOOK", "ABSTRACT", abstract_book[:-1])
        if keywords_book != current_book[8]:
            print("update KEYWORDS")
            self.sql.update_by_id(id_book, "BOOK", "KEYWORDS", keywords_book)
        if state_book != current_book[9]:
            print("update STATE")
            self.sql.update_by_id(id_book, "BOOK", "STATE", state_book)
        r = self.sql.list_books()
        self.format_book_list(r)
    def order_table(self, event):
        self.old_sort_index = self.sort_index
        self.sort_index = int(event[-1])
        if self.sort_index == self.old_sort_index:
            #sort_reverse xor True >>> alternating True/False/True/False
            self.sort_reverse = self.sort_reverse^True
        else:
            self.sort_reverse = False
        #print("index:" + str(self.sort_index) + ", direction:" + str(self.sort_direction))
        self.table_data.sort(key=lambda x: x[self.sort_index], reverse=self.sort_reverse)

    def format_book_list(self, r):
        #'REF','TITLE','LASTNAME','FIRSTNAME','EDITOR','STATE'
        r1 = [p[1:3]+p[4:7]+(p[-1],) for p in r]
        self.table_fulldata = r   #all fields in CATALOG
        self.table_data = r1      #only fields for table
    def filter_book_list(self):
        f_ref = self.main_window.Element("fil_ref").Get()
        f_title = self.main_window.Element("fil_tit").Get()
        f_lastname = self.main_window.Element("fil_las").Get()
        f_firstname = self.main_window.Element("fil_fir").Get()
        f_keywords = self.main_window.Element("fil_key").Get()
        f_abstract = self.main_window.Element("fil_abs").Get()
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        r = self.sql.list_books(ref=f_ref, title=f_title,
                                author_firstname=f_firstname,
                                author_lastname=f_lastname,
                                abstract=f_abstract,
                                keywords=f_keywords)
        self.format_book_list(r)


if __name__ == '__main__':
    #change curdir to parent folder of catalog.py:
    #print("Maintenant je suis à:", os.getcwd())
    module_path , module_file = os.path.split(__file__)
    parent_path = os.path.join(module_path,'..')
    os.chdir(parent_path)

    #print("Maintenant je suis à:", os.getcwd())

    logging.basicConfig(filename="bibou.log", level=logging.INFO,
                        format='%(asctime)s %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    
    Catalog(True)
else:
    pass
