#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bibou new book: graphic user interface for creating new books in the library
Created on Mon Nov 16 

@author: redondo
"""
import itertools
import json
import re

if __package__ is None or __package__ == "":
    import SQLconnector as b_sq
    from common import *
    import author_search as b_as
    import editor_search as b_es
    import internet_book_search as b_ibs
else:
    from . import SQLconnector as b_sq
    from .common import *
    from . import author_search as b_as
    from . import editor_search as b_es
    from . import internet_book_search as b_ibs

class NewBookIsbn:
    '''
        New book form using internet ISBN search
    '''
    def __init__(self,config):
        self.config = config
        self.database_file = self.config['Data']['database_file']
        if not os.path.isfile(self.database_file):
            logging.error("Database file not found")
            return
        self.sql = b_sq.SQLconnector(self.database_file)
        self.main_loop()

    def main_loop(self):
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            if event is None or event == 'exit':
                    break
            if event == 'OK':
                print("VERIFIER LES INFORMATIONS ET INSERER LE LIVRE")
                ref0 = self.main_window.Element("inp_ref").Get()
                title0 = self.main_window.Element("inp_tit").Get()
                isbn0 = self.main_window.Element("inp_isbn").Get()
                author0 = self.main_window.Element("inp_aut").Get()
                editor0 = self.main_window.Element("inp_edi").Get()
                abstract0 = self.main_window.Element("inp_abs").Get()
                keywords0 = self.main_window.Element("inp_key").Get()
                if ref0 and title0 and author0 and editor0:
                    print("essayons d'introduire ce livre dans la bibliothèque")
                    r = self.sql.list_books(ref=ref0)
                    if r:
                        PopupErreur("Un livre avec la même REF existe déjà",
                                title="Nouveau livre")
                    #TODO: check REF format (AA0000)
                    #TODO check author0 format ("ID: LAST, FIRST; ID2: etc.")
                    #TODO check editor0 format ("ID: NAME)
                    else:
                        aut_ids = self.insert_authors(author0)
                        edi_id = self.insert_editor(editor0)
                        fields = dict()
                        fields['REF'] = ref0
                        #replace apostrophs by double apostrophs to avoid errors
                        fields['TITLE'] = title0.replace("'","''")
                        #keep only digits in ISBN
                        fields['ISBN'] = re.sub('\D','',isbn0)
                        fields['ID_AUTHOR'] = aut_ids[0]
                        fields['ID_EDITOR'] = edi_id
                        #replace apostrophs by double apostrophs to avoid errors
                        fields['ABSTRACT'] = abstract0.replace("'","''")
                        fields['KEYWORDS'] = keywords0.replace("'","''")
                        print(fields)
                        #TODO error management in each sql operation
                        #insert book
                        self.sql.ins_book(fields)
                        #get id of new book
                        r = self.sql.get_book_by_ref(ref0)
                        id_book = r[0]
                        #update ID_OTHER_AUTHORS
                        id_others = json.dumps(aut_ids[1:])
                        self.sql.update_by_id(id_book,'BOOK','ID_OTHER_AUTHORS',id_others)
                        message = "Nouvelle fiche crée:\n" +\
                                    "REF: " + ref0 + "\n" +\
                                    "TITRE: "  + title0
                        PopupInfo(message,title="Nouveau livre")
                        break
                elif not ref0:
                    PopupErreur("Le champ REF est obligatoire",
                                title="Nouveau livre")
                elif not title0:
                    PopupErreur("Le champ TITRE est obligatoire",
                                title="Nouveau livre")
                elif not author0:
                    PopupErreur("Le champ AUTEUR est obligatoire",
                                title="Nouveau livre")
                else:
                    PopupErreur("Le champ EDITEUR est obligatoire",
                                title="Nouveau livre")
            if event == 'inp_ref':
                print("REF suggestions")
            if event == 'isbn_search':
                print("ISBN search on the internet")
                isbn0 = self.main_window.Element("inp_isbn").Get()
                b = b_ibs.bnf_isbn_search(isbn0)
                if not isbn0:
                    titles = []
                    aut_strings = []
                    edi_strings = []
                    abs_string = 'champ ISBN vide'
                elif b == -1:
                    titles = []
                    aut_strings = []
                    edi_strings = []
                    abs_string = 'Pas de connexion internet'
                elif b:
                    titles = [b1['title'] for b1 in b]
                    titles = list(itertools.chain.from_iterable(titles))
                    authors = [b1['author'] for b1 in b]
                    print('authors are')
                    print(authors)
                    aut_strings = [self.check_author_list(a) for a in authors]
                    editors = [b1['editor'] for b1 in b]
                    print('editors are')
                    print(editors)
                    edi_strings = [self.check_editor_list(e) for e in editors]
                    edi_strings = list(itertools.chain.from_iterable(edi_strings))
                    abs_string = ''
                else:
                    titles = []
                    aut_strings = []
                    edi_strings = []
                    abs_string = 'ISBN "' + isbn0 + '" non trouvé dans bnf.fr'
                self.main_window\
                    .Element("lst_tit")\
                    .Update(values=titles)
                self.main_window\
                    .Element("lst_aut")\
                    .Update(values=aut_strings)
                self.main_window\
                    .Element("lst_edi")\
                    .Update(values=edi_strings)
                self.main_window\
                    .Element("inp_abs")\
                    .Update(value=abs_string)
            if event == 'aut_search':
                print("open author form")
                U = b_as.AuthorSearch(self.config,True)
                if U.current_author:
                    print("Auteur:", U.current_author)
                    self.main_window\
                        .Element("inp_aut")\
                        .Update(str(U.current_author[0]) + ": " + U.current_author[1] + ", " + U.current_author[2])
            if event == 'edi_search':
                print("open editor form")
                U = b_es.EditorSearch(self.config,True)
                if U.current_editor:
                    print("Editeur:", U.current_editor)
                    self.main_window\
                        .Element("inp_edi")\
                        .Update(str(U.current_editor[0]) + ": " + U.current_editor[1])
            if event == 'lst_tit':
                    title0 = self.main_window.Element("lst_tit").Get()
                    self.main_window\
                        .Element("inp_tit")\
                        .Update(title0)
            if event == 'lst_aut':
                    author0 = self.main_window.Element("lst_aut").Get()
                    self.main_window\
                        .Element("inp_aut")\
                        .Update(author0)
            if event == 'lst_edi':
                    editor0 = self.main_window.Element("lst_edi").Get()
                    self.main_window\
                        .Element("inp_edi")\
                        .Update(editor0)
            print(event, values)
        self.main_window.Close()

    def make_main_window(self):
        #SAME DESIGN AS Catalog.book_window
        print("make main window")
        #Buttons
        bt_isbn_search = sg.Button(key="isbn_search", button_text="chercher ISBN")
        bt_author_search = sg.Button(key="aut_search", button_text="chercher auteur")
        bt_editor_search = sg.Button(key="edi_search", button_text="chercher editeur")
        bt_ok = sg.Button(key="OK", button_text="CREER LIVRE")
        #bt_undo = sg.Button(key="undo", button_text="Défaire modifications")
        bt_exit = sg.Button(key="exit", button_text="ANNULER")
        #bt_up = sg.Button(key="up", button_text="Précédent")
        #bt_down = sg.Button(key="down", button_text="Suivant")

        #Text Elements
        tx = sg.Text("NOUVEAU LIVRE ",
                     key="tx_heading",
                     size=(25, 1),
                     font=big_font,
                     background_color=skyblue)
        #Inputtext Elements
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        input_ref = sg.InputText('', size=(10, 1),
                                 disabled=False,
                                 key="inp_ref", enable_events=True, do_not_clear=True)
        input_tit = sg.InputText('', size=(100, 1),
                                 disabled=False,
                                 key="inp_tit", enable_events=True, do_not_clear=True)
        input_isbn = sg.InputText('', size=(15, 1),
                                  disabled=False,
                                  key="inp_isbn", enable_events=True, do_not_clear=True)
        #input_las = sg.InputText(current_book[4], size=(30, 1),
        #                         key="inp_las", enable_events=True, do_not_clear=True)
        #input_fir = sg.InputText(current_book[5], size=(30, 1),
        #                         key="inp_fir", enable_events=True, do_not_clear=True)
        #TODO enable author / editor edition,
        # but for this format ( "ID: LAST, FIRST ; etc.") must be checked before
        # event == 'OK'
        input_aut = sg.InputText('', size=(80, 1),
                                 disabled=True, key="inp_aut", 
                                 enable_events=True, do_not_clear=True)
        input_edi = sg.InputText('', size=(80, 1),
                                 disabled=True, key="inp_edi",
                                 enable_events=True, do_not_clear=True)         
        input_abs = sg.Multiline('', size=(100, 10),
                                 disabled=False,
                                 key="inp_abs", enable_events=True, do_not_clear=True)
        input_key = sg.InputText('', size=(100, 1),
                                 disabled=False,
                                 key="inp_key", enable_events=True, do_not_clear=True)
        #Images:
        im_left,im_right,im_top,im_top2 = take_images()
        #List Elements
        list_tit = sg.Combo([],
                            size=(80, 1), key="lst_tit",enable_events=True)
        list_aut = sg.Combo([],
                            size=(80, 1), key="lst_aut",enable_events=True)
        list_edi = sg.Combo([],
                            size=(80, 1), key="lst_edi",enable_events=True)

        #Text Elements
        tx_ref = sg.Text('REF', key="tx_ref", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #REF
        tx_tit = sg.Text('TITRE', key="tx_tit", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #TITLE
        tx_tit1 = sg.Text('', size=(30, 1), font=medium_font,
                          background_color=skyblue)  #TITLE (EMPTY TEXT)
        tx_isbn = sg.Text('ISBN', key="tx_isbn", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #ISBN
        #tx_las = sg.Text('AUTEUR (NOM)', size=(20, 1), font=medium_font,
        #                 background_color=skyblue)  #LASTNAME
        #tx_fir = sg.Text('AUTEUR (PRENOM)', size=(20, 1), font=medium_font,
        #                 background_color=skyblue)  #FIRSTNAME
        tx_aut = sg.Text('AUTEUR (ID/NEW: NOM, PRENOM)', key="tx_aut", size=(30, 1), font=medium_font,
                          background_color=skyblue)  #AUTHOR
        tx_aut1 = sg.Text('', size=(30, 1), font=medium_font,
                          background_color=skyblue)  #AUTHOR (EMPTY TEXT)
        tx_edi = sg.Text('EDITEUR (ID/NEW: NOM)', key="tx_edi", size=(30, 1), font=medium_font,
                         background_color=skyblue)  #EDITOR
        tx_edi1 = sg.Text('', size=(30, 1), font=medium_font,
                          background_color=skyblue)  #AUTHOR (EMPTY TEXT)
        tx_abs = sg.Text('RESUME', key="tx_abs", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #ABSTRACT
        tx_key = sg.Text('MOTS CLE', key="tx_key", size=(10, 1), font=medium_font,
                         background_color=skyblue)  #KEYWORDS
        tx_sta = sg.Text('ETAT', key="tx_sta", size=(5, 1), font=medium_font,
                         background_color=skyblue)  #STATE

        col = [[tx, ],
               [im_top2],
               [tx_ref, input_ref,
                tx_isbn, input_isbn, bt_isbn_search],
               [tx_tit, input_tit],
               [tx_tit1, list_tit],
               #[tx_las, input_las],
               #[tx_fir, input_fir],
               [tx_aut, input_aut],
               [tx_aut1, list_aut],
               [tx_edi, input_edi],
               [tx_edi1, list_edi],
               [tx_key, input_key],
               [tx_abs, input_abs],
               [im_top2],
               [bt_ok, bt_exit]]
        
        # TODO how to control column width? size=(650,768) is not working properly...
        layout = [[im_left,
                   sg.Column(col, background_color=skyblue, size=(None, None)),
                   im_right]]
        # Create the Window
        self.main_window = sg.Window("Fiche de livre", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
 
    def check_author_list(self,authors):
        #authors = [(lastname1, firstname1), (lastname2, firstname2)...]
        aut_strings = []
        for a in authors:
            r = self.sql.list_authors(firstname=a[1], lastname=a[0])
            if r:
                print("voici les résultats pour '" + a[1] + "' '" + a[0] + "'")
                print(r)
                #ID: LASTNAME, FIRSTNAME
                aut_strings.append(str(r[0][0]) + ": " + r[0][2] + ", " + r[0][1])
            else:
                aut_strings.append("NEW: " + a[0] + ", " + a[1])
        #ID1: LASTNAME1, FIRSTNAME1; ID2: LASTNAME2, FIRSTNAME2 ...
        aut_string = '; '.join(aut_strings)
        return aut_string

    def check_editor_list(self,editors):
        #editors = ['name', 'name'...]
        edi_strings = []
        for e in editors:
            r = self.sql.list_editors(name=e)
            if r:
                print("voici les résulatts pour '" + e + "' '")
                print(r)
                edi_strings.append(str(r[0][0]) + ": " + r[0][1])
            else:
                edi_strings.append("NEW: " + e)
        return edi_strings

    def insert_authors(self, aut_string):
        authors = aut_string.split(';')
        ids_aut = list()
        for a in authors:
            (id_aut, aut_name) = a.strip().split(':')
            if id_aut == "NEW":
                (aut_last, aut_first) = aut_name.split(',')
                aut_last = aut_last.strip()
                aut_first = aut_first.strip()
                self.sql.ins_author(aut_first, aut_last)
                r = self.sql.list_authors(firstname=aut_first, lastname=aut_last)
                #TODO error management (r empty, len(r)=>1)
                ids_aut.append(r[0][0])
            else:
                ids_aut.append(int(id_aut))
        #print("Author's IDS are: ")
        #print(ids_aut)
        return ids_aut

    def insert_editor(self, edi_string):
        (id_edi, edi_name) = edi_string.split(':')
        if id_edi == "NEW":
            edi_name = edi_name.strip()
            self.sql.ins_editor(edi_name)
            r = self.sql.list_editors(name=edi_name)
            #TODO error management (r empty, len(r)=>1)
            id_edi = r[0][0]
        else:
            id_edi = int(id_edi)
        print("Editor's ID is: ")
        print(id_edi)
        return id_edi
    def update_window(self):
        print("update window")


if __name__ == '__main__':
    import sys
    #change curdir to parent folder of catalog.py:
    #print("Maintenant je suis à:", os.getcwd())
    module_path , module_file = os.path.split(__file__)
    parent_path = os.path.join(module_path,'..')
    os.chdir(parent_path)

    #print("Maintenant je suis à:", os.getcwd())

    logging.basicConfig(filename="bibou.log", level=logging.INFO,
                        format='%(asctime)s %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')

    config = configparser.ConfigParser()
    if not os.path.isfile('settings.cfg'):
        print("Settings file not found")
        sys.exit()
    print("On a trouvé le fichier settings")
    config.read('settings.cfg')
    NewBookIsbn(config)
else:
    pass
