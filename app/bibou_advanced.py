#!/usr/bin/env python3
# -*- coding: utf8 -*-

#TODO potential error: import a user list (three columns) with updated levels
# of existing users
#import bibou_modules.SQLconnector as b_sq
import bibou_modules.catalog as b_ca
import bibou_modules.user_search as b_us
import bibou_modules.external_data_tools as b_ext
from bibou_modules.common import *

class bibou_advanced:
    def __init__(self):
        self.config = configparser.ConfigParser()
        if not os.path.isfile('settings.cfg'):
            print("Settings file not found")
            return
        self.config.read('settings.cfg')
        self.database_file = self.config['Data']['database_file']
        if not os.path.isfile(self.database_file):
            print("Database file not found")
            return
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            if event is None or event == 'exit':
                break
            if event == 'books':
                print("modifier/ajouter des livres un par un")
                self.main_window.Hide()
                b_ca.Catalog(True)
                self.main_window.UnHide()
            if event == 'users':
                print("modifier/ajouter des élèves un par un")
                self.main_window.Hide()
                b_us.UserSearch(self.config,True)
                self.main_window.UnHide()
            if event == 'import_books':
                print("ajouter des livres à partir d'un fichier")
            if event == 'import_users':
                print("ajouter des élèves à partir d'un fichier")
                self.import_user_dialog()
                self.main_window.Hide()
                b_us.UserSearch(self.config,True)
                self.main_window.UnHide()
            if event == 'stats':
                print("statistiques")
            print(event, values)
        self.main_window.Close()
    def make_main_window(self):
        #Buttons:
        bt_books = sg.Button(key="books",
                             button_text="GESTION DES LIVRES",
                             button_color=(white,violet0),
                             border_width=2, size=(30, 3), font=medium_font)
        bt_users = sg.Button(key="users",
                             button_text="GESTION DES ELEVES",
                             button_color=(white,green0),
                             border_width=2, size=(30, 3), font=medium_font)

        #bt_import_books = sg.Button(key="import_books",
        #                            button_text="IMPORTER\nFICHIER DE LIVRES",
        #                            button_color=(white,blue1),
        #                            border_width=2, size=(30, 3), font=medium_font)

        #bt_import_users = sg.Button(key="import_users",
        #                            button_text="IMPORTER\nFICHIER D'ELEVES",
        #                            button_color=(white,green1),
        #                            border_width=2, size=(30, 3), font=medium_font)

        bt_stats = sg.Button(key="stats",
                             button_text="STATISTIQUES DE\nLA BIBLIOTHEQUE",
                             button_color=(white,orange0),
                             border_width=2, size=(30, 3), font=medium_font)

        bt_exit = sg.Button(key="exit", button_text="SORTIR",
                            button_color=(white,red0),
                            border_width=2, size=(30, 3), font=medium_font)



        # All the stuff inside your window

        im_left,im_right,im_top,im_top2 = take_images()
        frame_layout = [[im_top],
                        [bt_books, bt_users],
                        [bt_stats, bt_exit]]
        fra = sg.Frame('', frame_layout, title_color='blue',
                       background_color=skyblue, border_width=0)
        layout = [[im_left, fra, im_right]]
        # Create the Window
        self.main_window = sg.Window('BIBOU: INTERFACE AVANCEE',
                                    size=(1024, 768), background_color=skyblue,
                                    element_padding=((0, 0), (0, 0)),
                                    element_justification="center",
                                    text_justification="center",
                                    margins=(0, 0)).Layout(layout)

    def import_user_dialog(self):
        bt_import = sg.FileBrowse(key="export",
                                  target='input',
                                  button_text="Choisir fichier...",
                                  font=medium_font, #border_width=1,
                                  #size=(25, 3), pad=(0, 0),
                                  enable_events=True,
                                  file_types=(('Excel 97','*.xls'),))
        layout = [[sg.T('Ouvrir un fichier *.xls...')],
                  [sg.In(key='input')],
                  [bt_import, sg.OK()]]
        user_dialog_window = sg.Window("IMPORTER FICHIER D'ELEVES",
                                       element_padding=((0, 0), (0, 0)),
                                       element_justification="center",
                                       #background_color=skyblue,
                                       margins=(0, 0)).Layout(layout)
        while True:
            event, values = user_dialog_window.Read()
            if event is None or event == 'exit':
                self.selected_user = []
                self.current_user = []
                break
            if event == 'OK':
                path_name = values['input']
                n_cols = b_ext.test_xls_user_file(path_name)
                if n_cols==4:
                    print("faire un update avec", path_name)
                    b_ext.update_users(path_name,self.database_file)
                elif n_cols==3:
                    print("faire un import avec", path_name)
                    b_ext.import_users(path_name,self.database_file)
                PopupInfo("Liste importée de\n" + path_name,
                          title="Information")
                break
        user_dialog_window.Close()
if __name__ == '__main__':
    #change cur dir to folder containing bibou.py (__file__)
    module_path , module_file = os.path.split(__file__)
    os.chdir(module_path)
    bibou_advanced()
