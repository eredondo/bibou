#!/usr/bin/env python3
# -*- coding: utf8 -*-

import bibou_modules.SQLconnector as b_sq
import bibou_modules.catalog as b_ca
import bibou_modules.user_search as b_us
from bibou_modules.common import *

# TODO choice list for level in user_search?

class new_loan:
    def __init__(self, config, user_id):
        self.table_loans = []
        self.table_cart = []
        self.table_catalog0 = [] # elements in catalog minus elements in cart
        self.table_catalog = []
        self.config = config
        database_file = self.config['Data']['database_file']
        self.sql = b_sq.SQLconnector(database_file)
        self.MAX_LOAN = int(self.config['LoanRules']['MaxLoan'])
        self.LOAN_DURATION = int(self.config['LoanRules']['LoanDuration'])
        #ID, FIRST, LAST, LEVEL
        self.current_user = self.sql.get_user_by_id(user_id)
        print("Make loan window for "+ str(self.current_user[0])
              + ", " + self.current_user[1]
              + ", " + self.current_user[2]
              + ", " + str(self.current_user[3]))
        #MAIN LOOP
        self.main_loop()
    def main_loop(self):
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            #self.update_main_window()
            if event is None or event == 'exit':
                break
            if event == 'add_book':
                if values['tab_catalog']:
                    if len(self.table_cart)+len(self.table_loans)<self.MAX_LOAN:
                        print("add this book to panier")
                        index = values['tab_catalog'][0]
                        self.table_cart.append(self.table_catalog0[index])
                        self.update_main_window()
                    else:
                        message = "ERREUR: nombre maximal de prêts: "\
                                  + str(self.MAX_LOAN)
                        PopupErreur(message)
                        print("maximum number of book reached")
            if event == 'rm_book':
                if values['tab_cart']:
                    print("rm this book from panier")
                    index = values['tab_cart'][0]
                    self.table_cart.pop(index)
                    self.update_main_window()
            if event == 'lend_books':
                message = "Prêter " + str(len(self.table_cart))\
                          + " livres à " + self.current_user[1]\
                          + "?"
                ans = PopupOuiNon(message)
                print("il a dit: " + ans)
                if ans == 'OUI':
                    id_user = self.current_user[0]
                    print("ID_USER: " + str(id_user))
                    for p in self.table_cart:
                        book = self.sql.get_book_by_ref(p[0])
                        id_book = book[0]
                        print("ID_BOOK: " + str(id_book) + ", REF_BOOK: " + str(p[0]))
                        self.sql.lend_book(id_book,id_user)
                    self.make_loan_list()
                    self.make_book_list()
                    self.table_cart = []
                self.update_main_window()
            if event == 'rm_all':
                print("rm all books from panier")
                self.table_cart = []
                self.update_main_window()
            if event.startswith("fil"):
                print("filtrer")
                self.make_book_list()
                self.update_main_window()
            print(event, values)
        self.main_window.Close()
    def make_main_window(self):
        ''' Method for making a new loan window
            The windows is composed of three tables: current loans, cart and
            catalog.
            At the bottom of the window there are some input texts to filter the
            catalog (ref, title, keywords, abstract, author).
            At the very bottom there are five buttons:
            Add book from catalog to cart, remove book from cart, remove all
            books from cart, lend books in cart to the user and exit.
        '''
        #TABLES
        #TABLE LOANS
        #REF, TITLE, DATE_LOAN, DATE_RETURN, STATE, ID_USER
        headers = ['REF', 'TITRE', 'DATE PRET', 'DATE RETOUR', 'ETAT']
        col_ws = [8, 19, 13, 13, 5]
        TABLE_LOANS = make_sg_table("tab_loans",
                                    self.table_loans, headers, col_ws)
        #TABLE CART (same fields as catalog)
        headers = ['REF', 'TITRE', 'NOM AUTEUR', 'PRENOM AUT.', 'EDITEUR', 'ETAT']
        col_ws = [8, 15, 10, 10, 10, 5]
        TABLE_CART = make_sg_table("tab_cart",
                                    [], headers, col_ws)
        #TABLE CATALOG
        headers = ['REF', 'TITRE', 'NOM AUTEUR', 'PRENOM AUT.', 'EDITEUR', 'ETAT']
        col_ws = [8, 15, 10, 10, 10, 5]
        TABLE_CATALOG = make_sg_table("tab_catalog",
                                    self.table_catalog0, headers, col_ws, num_rows=10)
        #buttons
        bt_add_book = sg.Button(key="add_book", button_text="AJOUTER AU PANIER",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_rm_book = sg.Button(key="rm_book", button_text="ENLEVER DU PANIER",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_rm_all = sg.Button(key="rm_all", button_text="VIDER LE PANIER",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        bt_lend_books = sg.Button(key="lend_books",
                                  button_text="PRETER LES LIVRES DU PANIER",
                                  font=medium_font, border_width=1,
                                  size=(50, 3), pad=(0, 0))
        bt_exit = sg.Button(key="exit", button_text="SORTIR",
                              font=medium_font, border_width=1,
                              size=(25, 3), pad=(0, 0))
        #input texts (for catalog filtering)
        input_ref = sg.InputText(key="fil_ref", size=(10, 0),
                                 enable_events=True, do_not_clear=True)
        input_tit = sg.InputText(key="fil_tit", size=(40, 0),
                                 enable_events=True, do_not_clear=True)
        input_fir = sg.InputText(key="fil_fir", size=(15, 0),
                                 enable_events=True, do_not_clear=True)
        input_las = sg.InputText(key="fil_las", size=(15, 0),
                                 enable_events=True, do_not_clear=True)
        #Texts:
        tx_heading = sg.Text("NOUVEAU PRET A "\
                             + self.current_user[1] + " "\
                             + self.current_user[2],
                             key="tx_heading",
                             size=(35, 1),
                             font=big_font,
                             background_color=skyblue)
        tx_loans = sg.Text('PRETS EN COURS ('+ str(len(self.table_loans)) + ')',
                           key="tx_loans",
                           size=(80, 1), font=medium_font,
                           background_color=skyblue)
        tx_cart = sg.Text('PANIER('+ str(len(self.table_cart)) + ')',
                          key="tx_cart",
                          size=(80, 1), font=medium_font,
                          background_color=skyblue)
        tx_catalog = sg.Text('CATALOGUE('+ str(len(self.table_catalog0)) + ')',
                             key="tx_catalog",
                             size=(80, 1), font=medium_font,
                             background_color=skyblue)
        tx_ref = sg.Text('REF', size=(10, 1), font=medium_font,
                         background_color=skyblue)  #ref filter
        tx_tit = sg.Text('TITRE', size=(10, 1), font=medium_font,
                         background_color=skyblue)  #title filter
        tx_fir = sg.Text('PRENOM AUTEUR', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #firstname filter
        tx_las = sg.Text('NOM AUTEUR', size=(20, 1), font=medium_font,
                         background_color=skyblue)  #lastname filter
        #MIDDLE COLUMN
        im_left,im_right,im_top,im_top2 = take_images()
        #previous design
        col = [[tx_heading],
               [tx_loans],
               [TABLE_LOANS],
               [tx_cart],
               [TABLE_CART],
               [tx_catalog],
               [TABLE_CATALOG],
               [tx_ref, input_ref, tx_tit, input_tit],
               [tx_fir, input_fir, tx_las, input_las],
               [im_top2],
               [bt_add_book, bt_rm_book, bt_rm_all], [bt_lend_books, bt_exit]]
        #new design
        col = [[tx_heading],
               [tx_ref, input_ref, tx_tit, input_tit],
               [tx_fir, input_fir, tx_las, input_las],
               [tx_catalog],
               [TABLE_CATALOG],
               [bt_add_book, bt_rm_book, bt_rm_all],
               [tx_cart],
               [TABLE_CART],
               [tx_loans],
               [TABLE_LOANS],
               [im_top2],
               [bt_lend_books, bt_exit]]
        layout = [[im_left,
                   sg.Column(col,
                             background_color=skyblue,
                             size=(None, None)),
                   im_right]]
        # Create the Window
        self.main_window = sg.Window("NOUVEAU PRET", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
        #self.table_loans
        self.make_loan_list()
        #self.table_catalog
        self.make_book_list()
        self.update_main_window()
    def update_main_window(self):
        #remove books in catalog already in cart:
        self.table_catalog0 = [p for p in self.table_catalog if p not in self.table_cart]
        #LOANS
        TABLE_LOANS = self.main_window.Element("tab_loans")
        TABLE_LOANS.Update(values=self.table_loans)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE_LOANS.AlternatingRowColor = 'lightblue'
        TABLE_LOANS.RowColor = 'blue'
        #CATALOG
        TABLE_CATALOG = self.main_window.Element("tab_catalog")
        TABLE_CATALOG.Update(values=self.table_catalog0)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE_CATALOG.AlternatingRowColor = 'lightblue'
        TABLE_CATALOG.RowColor = 'blue'
        #CART
        TABLE_CART = self.main_window.Element("tab_cart")
        TABLE_CART.Update(values=self.table_cart)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE_CART.AlternatingRowColor = 'lightblue'
        TABLE_CART.RowColor = 'blue'
        #Update number of elements in texts describing tables:
        self.main_window.Element("tx_loans").Update('PRETS EN COURS ('+ str(len(self.table_loans)) + ')')
        self.main_window.Element("tx_cart").Update('PANIER('+ str(len(self.table_cart)) + ')')
        self.main_window.Element("tx_catalog").Update('CATALOGUE('+ str(len(self.table_catalog0)) + ')')
        
    def make_book_list(self):
        f_ref = self.main_window.Element("fil_ref").Get()
        f_tit = self.main_window.Element("fil_tit").Get()
        f_firstname = self.main_window.Element("fil_fir").Get()
        f_lastname = self.main_window.Element("fil_las").Get()
        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
        r = self.sql.list_books(ref=f_ref, title=f_tit,
                                author_firstname=f_firstname,
                                author_lastname=f_lastname,state=0)
        #'REF','TITLE','LASTNAME','FIRSTNAME','EDITOR','STATE'
        r1 = [p[1:3]+p[4:7]+(p[-1],) for p in r]
        self.table_catalog = r1      #only fields for table_catalog
        #remove books in catalog already in cart:
        self.table_catalog0 = [p for p in self.table_catalog if p not in self.table_cart]
#    def make_book_list0(self):
#        #(ID,REF,TITLE,ISBN,LASTNAME,FIRSTNAME,EDITOR,ABSTRACT,KEYWORDS,STATE)
#        r = self.sql.list_books(state=0)
#        #'REF','TITLE','LASTNAME','FIRSTNAME','EDITOR','STATE'
#        r1 = [p[1:3]+p[4:7]+(p[-1],) for p in r]
#        self.table_catalog = r1      #only fields for table_catalog
#        #remove books in catalog already in cart:
#        self.table_catalog0 = [p for p in self.table_catalog if p not in self.table_cart]
    def make_loan_list(self):
        r = self.sql.list_loans_by_user(id_user=self.current_user[0])
        #ID, ID_BOOK, ID_USER, DATE_LOAN, DATE_RETURN, REF, TITLE, STATE
        #REF, TITLE, DATE_LOAN, DATE_RETURN, STATE, ID_USER
        r1 = [p[5:7]+p[3:5]+(p[-1],)+(p[2],) for p in r]
        self.table_loans = r1      #only fields for table_loans
class new_return:
    def __init__(self, config, user_id):
        self.fulltable_loans = []
        self.table_loans = []
        self.table_loans0 = []   #books in loans not in cart
        self.table_cart = []
        self.config = config
        database_file = self.config['Data']['database_file']
        self.sql = b_sq.SQLconnector(database_file)
        #ID, FIRST, LAST, LEVEL
        self.current_user = self.sql.get_user_by_id(user_id)
        print("Make return window for "+ str(self.current_user[0])
              + ", " + self.current_user[1]
              + ", " + self.current_user[2]
              + ", " + str(self.current_user[3]))
        #MAIN LOOP
        self.main_loop()
    def main_loop(self):
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            #self.update_main_window()
            if event is None or event == 'exit':
                break
            if event == 'add_book':
                if values['tab_loans']:
                    print("add this book to panier")
                    index = values['tab_loans'][0]
                    self.table_cart.append(self.table_loans0[index])
                    self.update_main_window()
            if event == 'rm_book':
                if values['tab_cart']:
                    print("rm this book from panier")
                    index = values['tab_cart'][0]
                    self.table_cart.pop(index)
                    #self.make_loan_list()
                    self.update_main_window()
            if event == 'return_books':
                message = "Retour de " + str(len(self.table_cart))\
                          + " livres"\
                          + "?"
                ans = PopupOuiNon(message)
                if ans == 'OUI':
                    print("return these books to the library")
                    id_user = self.current_user[0]
                    print("ID_USER: " + str(id_user))
                    for p in self.table_cart:
                        id_loan = p[-1]
                        print("ID_LOAN: " + str(id_loan))
                        self.sql.return_book(id_loan)
                    self.make_loan_list()
                    self.table_cart = []
                self.update_main_window()
            if event == 'rm_all':
                print("rm all books from cart")
                self.table_cart = []
                self.update_main_window()
            if event == 'add_all':
                print("put all books in cart")
                self.table_cart = self.table_loans[:]
                self.update_main_window()
            print(event, values)
        self.main_window.Close()
    def make_main_window(self):
        ''' Method for making a new loan window
            The windows is composed of three tables: current loans, cart and
            catalog.
            At the bottom of the window there are some input texts to filter the
            catalog (ref, title, keywords, abstract, author).
            At the very bottom there are five buttons:
            Add book from catalog to cart, remove book from cart, remove all
            books from cart, lend books in cart to the user and exit.
        '''
        #TABLES
        #TABLE LOANS
        #REF, TITLE, DATE_LOAN, DATE_RETURN, STATE, ID_USER
        headers = ['REF', 'TITRE', 'DATE PRET', 'DATE RETOUR', 'ETAT', 'ID PRET']
        col_ws = [8, 19, 10, 10, 5, 6]
        TABLE_LOANS = make_sg_table("tab_loans",
                            self.table_loans0, headers, col_ws)
        #TABLE CART (same fields as catalog)
        headers = ['REF', 'TITRE', 'DATE PRET', 'DATE RETOUR', 'ETAT', 'ID PRET']
        col_ws = [8, 19, 10, 10, 5, 6]
        TABLE_CART = make_sg_table("tab_cart", [], headers, col_ws)
        #buttons
        bt_add_book = sg.Button(key="add_book", button_text="AJOUTER AU PANIER",
                              font=medium_font, border_width=1,
                              size=(30, 3), pad=(0, 0))
        bt_rm_book = sg.Button(key="rm_book", button_text="ENLEVER DU PANIER",
                              font=medium_font, border_width=1,
                              size=(30, 3), pad=(0, 0))
        bt_rm_all = sg.Button(key="rm_all", button_text="VIDER LE PANIER",
                              font=medium_font, border_width=1,
                              size=(30, 3), pad=(0, 0))
        bt_add_all = sg.Button(key="add_all", button_text="TOUT DANS LE PANIER",
                              font=medium_font, border_width=1,
                              size=(30, 3), pad=(0, 0))
        bt_return_books = sg.Button(key="return_books",
                                  button_text="RETOURNER\nLES LIVRES\nDU PANIER",
                                  font=medium_font, border_width=1,
                                  size=(30, 3), pad=(0, 0))
        bt_exit = sg.Button(key="exit", button_text="SORTIR",
                              font=medium_font, border_width=1,
                              size=(30, 3), pad=(0, 0))

        #Texts:
        tx_heading = sg.Text("Retour de livres prêtés à  "\
                             + self.current_user[1] + " "\
                             + self.current_user[2],
                             key="tx_heading",
                             size=(40, 1),
                             font=big_font,
                             background_color=skyblue)
        tx_loans = sg.Text('PRETS EN COURS ('+ str(len(self.table_loans0)) + ')',
                           key="tx_loans",
                           size=(50, 1), font=medium_font,
                           background_color=skyblue)
        tx_cart = sg.Text('PANIER('+ str(len(self.table_cart)) + ')',
                          key="tx_cart",
                          size=(50, 1), font=medium_font,
                          background_color=skyblue)
        #MIDDLE COLUMN
        im_left,im_right,im_top,im_top2 = take_images()
        col = [[tx_heading],
               [im_top2],
               [tx_loans],
               [TABLE_LOANS],
               [im_top2],
               [bt_add_book, bt_rm_book],
               [bt_add_all, bt_rm_all],
               [im_top2],
               [tx_cart],
               [TABLE_CART],
               [im_top2],
               [bt_return_books, bt_exit]]
        layout = [[im_left,
                   sg.Column(col,
                             background_color=skyblue,
                             size=(None, None)),
                   im_right]]
        # Create the Window
        self.main_window = sg.Window("RETOUR PRET", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
        #self.table_loans
        self.make_loan_list()
        self.update_main_window()
    def update_main_window(self):
        #remove books in loans already in cart:
        self.table_loans0 = [p for p in self.table_loans if p not in self.table_cart]
        #LOANS
        TABLE_LOANS = self.main_window.Element("tab_loans")
        TABLE_LOANS.Update(values=self.table_loans0)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE_LOANS.AlternatingRowColor = 'lightblue'
        TABLE_LOANS.RowColor = 'blue'
        #CART
        TABLE_CART = self.main_window.Element("tab_cart")
        TABLE_CART.Update(values=self.table_cart)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE_CART.AlternatingRowColor = 'lightblue'
        TABLE_CART.RowColor = 'blue'
        #Update number of elements in texts describing tables:
        self.main_window.Element("tx_loans").Update('PRETS EN COURS ('+ str(len(self.table_loans)) + ')')
        self.main_window.Element("tx_cart").Update('PANIER('+ str(len(self.table_cart)) + ')')
    def make_loan_list(self):
        r = self.sql.list_loans_by_user(id_user=self.current_user[0])
        self.fulltable_loans = r
        #ID, ID_BOOK, ID_USER, DATE_LOAN, DATE_RETURN, REF, TITLE, STATE
        #REF, TITLE, DATE_LOAN, DATE_RETURN, STATE, ID_LOAN
        r1 = [p[5:7]+p[3:5]+(p[-1],)+(p[0],) for p in r]
        self.table_loans = r1      #only fields for table_loans

class current_loans:
#TODO: INPUT TEXT FOR FILTERING (title, ref, firstname, lastname, etc.
#TODO: IN RED COLOR IF DATE OF RETURN IS PASSED
    def __init__(self, config):
        self.fulltable_loans = []
        self.table_loans = []
        self.fulltable_loans2 = []
        self.table_loans2 = []
        self.config = config
        self.sort_index = -1
        self.sort_direction = 1    #see order_table method
        database_file = self.config['Data']['database_file']
        self.sql = b_sq.SQLconnector(database_file)
        #MAIN LOOP
        self.main_loop()
    def main_loop(self):
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            #self.update_main_window()
            if event is None or event == 'exit':
                break
            if event.startswith("tri"):
                print("FAIRE LE TRI")
                self.order_table(event)
                self.update_main_window()
        self.main_window.Close()
    def make_main_window(self):
        ''' Method for making a new loan window
            The window is composed of one table: current loans
            At the bottom of the window there is a button to close the window.
        '''
        #TABLES
        #TABLE LOANS
        #ID, DATE_LOAN, DATE_RETURN, RETURNED [0 1 2 3]
        #REF, TITLE, STATE                    [4 5 6]
        #ID_USER, FIRSTNAME, LASTNAME, LEVEL  [7 8 9 10]
        headers = ['No PRET', 'PRET', 'RETOUR', 'RETOURNE?',
                   'REF', 'TITRE', 'ETAT',
                   'ID', 'PRENOM', 'NOM', 'NIVEAU']
        col_ws = [6, 8, 8, 9, 8, 20, 5, 5, 9, 9, 6]
        headers = ['Nº PRET', 'DATE DE PRET', 'DATE DE RETOUR',
                   'REF', 'TITRE',
                   'PRENOM', 'NOM', 'NIVEAU']
        col_ws = [6, 12, 12, 8, 29, 9, 9, 6]
        TABLE_LOANS = make_sg_table("tab_loans", self.table_loans,
                                   headers, col_ws, num_rows=15)
        TABLE_LOANS2 = make_sg_table("tab_loans2", self.table_loans2,
                                   headers, col_ws, num_rows=10)
        #buttons
        bt_exit = sg.Button(key="exit", button_text="SORTIR",
                              font=medium_font, border_width=1,
                              size=(30, 3), pad=(0, 0))
        K = 1.3
        bt_tri_0 = sg.Button(key="tri_0", button_text=headers[0],
                               font=medium_font, border_width=1,
                               size=(int(col_ws[0]*K), 0), pad=(0, 0))
        bt_tri_1 = sg.Button(key="tri_1", button_text=headers[1],
                               font=medium_font, border_width=1,
                               size=(int(col_ws[1]*K), 0), pad=(0, 0))
        bt_tri_2 = sg.Button(key="tri_2", button_text=headers[2],
                               font=medium_font, border_width=1,
                               size=(int(col_ws[2]*K), 0), pad=(0, 0))
        bt_tri_3 = sg.Button(key="tri_3", button_text=headers[3],
                               font=medium_font, border_width=1,
                               size=(int(col_ws[3]*K), 0), pad=(0, 0))
        bt_tri_4 = sg.Button(key="tri_4", button_text=headers[4],
                               font=medium_font, border_width=1,
                               size=(int(col_ws[4]*K), 0), pad=(0, 0))
        bt_tri_5 = sg.Button(key="tri_5", button_text=headers[5],
                               font=medium_font, border_width=1,
                               size=(int(col_ws[5]*K), 0), pad=(0, 0))
        bt_tri_6 = sg.Button(key="tri_6", button_text=headers[6],
                               font=medium_font, border_width=1,
                               size=(int(col_ws[6]*K), 0), pad=(0, 0))
        bt_tri_7 = sg.Button(key="tri_7", button_text=headers[7],
                               font=medium_font, border_width=1,
                               size=(int(col_ws[7]*K), 0), pad=(0, 0))
        #Texts:
        tx_heading = sg.Text("PRETS EN COURS (" + str(len(self.table_loans)) + ")",
                             key="tx_heading",
                             size=(35, 1),
                             font=big_font,
                             background_color=skyblue)
        tx_heading2 = sg.Text("PRETS EN RETARD (" + str(len(self.table_loans2)) + ")",
                             key="tx_heading2",
                             size=(35, 1),
                             font=big_font,
                             background_color=skyblue)
        #MIDDLE COLUMN
        im_left,im_right,im_top,im_top2 = take_images()
        col = [[tx_heading],
               [TABLE_LOANS],
               [bt_tri_0, bt_tri_1, bt_tri_2, bt_tri_3,
                bt_tri_4, bt_tri_5, bt_tri_6, bt_tri_7],
               [im_top2],
               [tx_heading2],
               [TABLE_LOANS2],
               [im_top2],
               [bt_exit]]
        layout = [[sg.Column(col,
                             background_color=skyblue,
                             size=(None, None))]]
        # Create the Window
        self.main_window = sg.Window("PRETS EN COURS", size=(1024, 768),
                                     background_color=skyblue,
                                     element_padding=((0, 0), (0, 0)),
                                     element_justification="center",
                                     margins=(0, 0)).Layout(layout).Finalize()
        #self.table_loans
        self.make_loan_list()
        self.update_main_window()
    def update_main_window(self):
        #LOANS
        TABLE_LOANS = self.main_window.Element("tab_loans")
        TABLE_LOANS.Update(values=self.table_loans)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE_LOANS.AlternatingRowColor = 'lightblue'
        TABLE_LOANS.RowColor = 'blue'
        #DELAYED LOANS
        TABLE_LOANS2 = self.main_window.Element("tab_loans2")
        TABLE_LOANS2.Update(values=self.table_loans2)
        #FIXME not working
        #TABLE.Update(select_rows=[self.current_book_index])
        TABLE_LOANS2.AlternatingRowColor = 'lightblue'
        TABLE_LOANS2.RowColor = 'blue'
        #TEXTS
        tx = "PRETS EN COURS (" + str(len(self.table_loans)) + ")"
        tx2 = "PRETS EN RETARD (" + str(len(self.table_loans2)) + ")"
        self.main_window.Element("tx_heading").Update(tx)
        self.main_window.Element("tx_heading2").Update(tx2)

    def make_loan_list(self):
        r = self.sql.list_full_loans_info(returned=0)
        self.fulltable_loans = r
        #ID, DATE_LOAN, DATE_RETURN, RETURNED [0 1 2 3]
        #REF, TITLE, STATE                    [4 5 6]
        #ID_USER, FIRSTNAME, LASTNAME, LEVEL  [7 8 9 10]
        r1 = [p[0:3]+p[4:6]+p[8:] for p in r]
        self.table_loans = r1      #only fields for table_loans
        today = str(datetime.date.today())    #TESTS: +datetime.timedelta(13))
        r = self.sql.list_full_loans_info(returned=0,
                                          to_date_return=today)
        self.fulltable_loans2 = r
        #ID, DATE_LOAN, DATE_RETURN, RETURNED [0 1 2 3]
        #REF, TITLE, STATE                    [4 5 6]
        #ID_USER, FIRSTNAME, LASTNAME, LEVEL  [7 8 9 10]
        r1 = [p[0:3]+p[4:6]+p[8:] for p in r]
        self.table_loans2 = r1      #only fields for table_loans
    def order_table(self, event):
        self.old_sort_index = self.sort_index
        self.sort_index = int(event[-1])
        if self.sort_index == self.old_sort_index:
            #sort_reverse xor True >>> alternating True/False/True/False
            self.sort_reverse = self.sort_reverse^True
        else:
            self.sort_reverse = False
        #print("index:" + str(self.sort_index) + ", direction:" + str(self.sort_direction))
        self.table_loans.sort(key=lambda x: x[self.sort_index], reverse=self.sort_reverse)

class bibou:
    def __init__(self):
        self.config = configparser.ConfigParser()
        if not os.path.isfile('settings.cfg'):
            print("Settings file not found")
            return
        self.config.read('settings.cfg')
        #TODO check config is OK (e.g. integer values for loan rules, valid fonts, etc.)
        self.database_file = self.config['Data']['database_file']
        if not os.path.isfile(self.database_file):
            print("Database file not found")
            return
        self.make_main_window()
        while True:
            event, values = self.main_window.Read()
            if event is None or event == 'exit':
                break
            if event == 'new':
                print("new loan")
                self.main_window.Hide()
                #U = user_search(self.config)
                U = b_us.UserSearch(self.config)
                if U.current_user:
                    new_loan(self.config,U.current_user[0])
                self.main_window.UnHide()
            if event == 'return':
                print("return book")
                self.main_window.Hide()
                U = b_us.UserSearch(self.config)
                if U.current_user:
                    new_return(self.config,U.current_user[0])
                self.main_window.UnHide()
            if event == 'loans':
                print("existing loans")
                self.main_window.Hide()
                current_loans(self.config)
                self.main_window.UnHide()
            if event == 'catalog':#TODO
                self.main_window.Hide()
                b_ca.Catalog(False)
                self.main_window.UnHide()
            print(event, values)
        self.main_window.Close()
    def make_main_window(self):
        #Buttons:
        bt_new = sg.Button(key="new", button_text="",
                           button_color=(skyblue, skyblue),
                           border_width=0, image_filename="images/bt_new.png",
                           image_size=(200, 200), font=big_font)
        bt_return = sg.Button(key="return", button_text="",
                              button_color=(skyblue, skyblue),
                              border_width=0, image_filename="images/bt_ret.png",
                              image_size=(200, 200), font=big_font)
        bt_loans = sg.Button(key="loans", button_text="",
                             button_color=(skyblue, skyblue),
                             border_width=0, image_filename="images/bt_cours.png",
                             image_size=(200, 200), font=big_font)
        bt_catalog = sg.Button(key="catalog", button_text="",
                             button_color=(skyblue, skyblue),
                             border_width=0, image_filename="images/bt_cat.png",
                             image_size=(200, 200), font=big_font)
        bt_exit = sg.Button(key="exit", button_text="",
                            button_color=(skyblue, skyblue),
                            border_width=0, image_filename="images/bt_exit.png",
                            image_size=(200, 200), font=big_font)

        # All the stuff inside your window
        tx_blank = sg.Text('', size=(20, 1), background_color=skyblue)

        im_left,im_right,im_top,im_top2 = take_images()
        frame_layout = [[im_top],
                        [bt_new, bt_return],
                        [bt_loans, bt_catalog, bt_exit]]
        fra = sg.Frame('', frame_layout, font='courier 12', title_color='blue',
                       background_color=skyblue, border_width=0)
        layout = [[im_left, fra, im_right]]
        # Create the Window
        self.main_window = sg.Window('BIBOU: GESTION DE PRETS DE LA BIBLIOTHEQUE',
                                    size=(1024, 768), background_color=skyblue,
                                    element_padding=((0, 0), (0, 0)),
                                    element_justification="center",
                                    margins=(0, 0)).Layout(layout)

if __name__ == '__main__':
    #change cur dir to folder containing bibou.py (__file__)
    module_path , module_file = os.path.split(__file__)
    os.chdir(module_path)
    bibou()
