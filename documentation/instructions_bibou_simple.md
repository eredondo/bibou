# <a name="home"></a> Manuel: Gestion des prêts avec bibou (interface simple).

L'écran principal de bibou contient cinq boutons:

- [Nouveau prêt](#loan)
- [Retour prêt](#return)
- [Prêts en cours](#current)
- [Catalogue](#catalog)
- Sortir

![Bibou: fenêtre principale](./screenshots/00_bibou_simple.png  "Bibou: fenêtre principale")


## <a name="loan"></a>Nouveau prêt [⬆](#home)

En cliquant sur le bouton **Nouveau prêt**, la **fenêtre de recherche d'utilisateurs** s'ouvre.
Dans cette fenêtre, nous pouvons chercher un utilisateur (quelqu'un qui emprunte des livres à la bibliothèque), soit en parcourant la liste, soit en utilisant les champs de texte **PRENOM**, **NOM**, **NIVEAU** pour filtrer la liste:

![recherche d'élèves](./screenshots/10_user_search.png  "recherche d'élèves")

Par exemple, on peut filtrer par prénom. Ici, nous avons écrit "AU" dans le champ **PRENOM**. Seulement les utilisateurs dont le prénom contient "AU" sont listés:

![filtrage d'élèves par prénom](./screenshots/11_user_search_firstname_filter.png  "filtrage d'élèves par prénom")

On peut aussi écrire un chiffre dans le champ **NIVEAU**. Ici nous avons filtré tous les utilisateurs du niveau 2:

![filtrage d'élèves par niveau](./screenshots/12_user_search_level_filter.png  "filtrage d'élèves par niveau")

Ensuite, lorsqu'on clique sur **SELECTIONNER**, la **fenêtre de nouveau prêt** apparaît, pour faire un prêt à l'utilisateur sélectionné.
Par exemple, ci-dessous on voit la fenêtre de nouveau prêt à Quenza Durand.

![nouveau prêt](./screenshots/20_new_loan.png  "nouveau prêt")

Cette fenêtre contient trois listes:
- les prêts en cours de l'utilisateur (dans l'exemple deux livres sont déjà empruntés par l'utilisatrice)
- le panier (initialement vide, nous allons y déposer des livres pour les prêter)
- la liste de livres disponibles

D'abord nous pouvons rechercher un livre en utilisant les filtres. Par exemple, en écrivant "luck" dans le champ **TITRE**, seulement des BD's de Lucky Luke aparaîsent:

![nouveau prêt, filtrage par titre](./screenshots/21_new_loan_title_filter.png  "nouveau prêt, filtrage par titre")

Ensuite nous pouvons sélectionner des livres pour les mettre dans le panier en utilisant le bouton **AJOUTER AU PANIER**:

![nouveau prêt, panier](./screenshots/22_new_loan_cart.png  "nouveau prêt, panier")

Nous pouvons ajouter des livres au panier jusqu'à une certaine limite. Par exemple, ici le nombre maximal de prêts par utilisateur est de 5, comme l'utilisatrice a déjà emprunté deux, le message d'erreur suivant apparaît dès que nous essayons de mettre un quatrième livre dans le panier:

![nouveau prêt, nombre maximum de prêts](./screenshots/23_max_loan_number.png  "nouveau prêt, nombre maximum de prêts")

Une fois nous sommes sûrs des livres qui seront prêtés, nous pouvons cliquer sur le bouton **PRETER LES LIVRES DU PANIER** et une fenêtre de dialogue apparaît:

![nouveau prêt, confirmation de prêt](./screenshots/24_loan_confirmation.png  "nouveau prêt, confirmation de prêt")

Si la réponse est positive, les livres du panier passeront à ête prêtés, c'est-à-dire, à la liste de prêts en cours de l'utilisateur:

![nouveau prêt, prêt réalisé](./screenshots/25_after_loan.png  "nouveau prêt, prêt réalisé")

## <a name="return"></a>Retour de livres [⬆](#home)
Comme pour les [nouveaux prêts](#loan), en cliquant sur le bouton **Retour prêt**, la **fenêtre de recherche d'utilisateurs** s'ouvre pour que l'on puisse choisir un utilisateur. Une fois l'utilisateur sélectionné, la **fenêtre de retour de prêts** pour l'utilisateur en question apparaît:

![retour de livres](./screenshots/30_new_return.png  "retour de livres")

Cette fenêtre contient deux listes:
- les prêts en cours de l'utilisateur
- le panier
Ici il s'agit de mettre des livres dans le panier pour les restituer à la bibliothèque. Nous pouvons les sélectionner un par un (**AJOUTER AU PANIER**) pour les mettre dans le panier ou pour les sortir (**ENLEVER DU PANIER**); ou nous pouvons tout mettre ou tout sortir dans le panier (**TOUT DANS LE PANIER**, **VIDER LE PANIER**):

![retour de livres: panier](./screenshots/31_new_return_cart.png  "retour de livres: panier")

Une fois que les livres qui seront retournés sont le panier, en cliquant sur le bouton **RETOURNER LES LIVRES DU PANIER**, la fenêtre de confirmation apparaît:

![retour de livres: confirmation](./screenshots/32_new_return_confirmation.png  "retour de livres: confirmation")

Si la réponse est positive le retour de livres sera effectué (les livres deviendront disponibles au prêt).

## <a name="current"></a>Prêts en cours [⬆](#home)
En cliquant sur le bouton **Prêts en cours**, nous pouvons visualiser tous les livres qui sont en prêt actuellement (liste d'en haut).
Une deuxième liste (liste d'en bas) montre seulement les livres qui sont en retard:

![prêts en cours](./screenshots/40_current_loans.png  "prêts en cours")

## <a name="catalog"></a>Catalogue [⬆](#home)
Nous pouvons aussi accéder au catalogue de la bibliothèque:

![catalogue (mode lecture)](./screenshots/50_catalog_read_only.png  "catalogue (mode lecture)")

On peut utiliser les champs de texte pour filtrer la liste, par exemple par nom d'auteur:

![catalogue: filtrage par nom d'auteur](./screenshots/51_catalog_lastname_filter.png  "catalogue: filtrage par nom d'auteur")

Les boutons situés juste en dessous de la liste servent à trier selon les valeurs de la colonne en question (premier clic ordre croissant, deuxième clic ordre décroissant):

![catalogue: tri par titre](./screenshots/52_catalog_title_order.png  "catalogue: tri par titre")

Nous pouvons accéder au détail de chaque livre en cliquant sur le bouton **Fiche du livre**:

![catalogue: fiche de livre](./screenshots/53_catalog_book_form.png  "catalogue: fiche de livre")

Dans cette fenêtre nous avons accès à quelques champs supplémentaires (mots clé, résumé, état).
L'état du livre est un code numérique:
- **0** livre disponible
- **1** livre prêté
- **-1** livre non disponible en prêt (consultable en salle uniquement)
- **-2** livre non disponible (en réparation?, perdu?, etc.)

Avec les boutons **Précédent** et **Suivant** nous pouvons naviguer dans la liste (ici nous avions filtré 15 livres de Goscinny).

