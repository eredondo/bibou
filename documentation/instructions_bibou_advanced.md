# Instructions d'utilisation de **bibou advanced**

Cette interface d'utilisation est destinée aux adminitrateurs de la bibliothèque:
elle permet l'accès en lecture et en écriture des données du catalogue et des utilisateurs.
On peut donc creer/modifier des fiches de livres et des utilisateurs.
La fenêtre principale contient quatre boutons:

- Gestion des livres
- Gestion des élèves
- Statistiques de la bibliothèque
- Sortir

## Gestion des livres
En cliquant sur ce bouton nous pouvons accéder au cataogue de la bibliothèque mais,
à différence de **bibou simple**, ici nous avons un accès en écriture,
c-à-d nous pouvons ajouter des fiches et modifier les fiches existantes.

Pour filtrer les livres montrés dans la liste on peut utiliser les champs de texte **Ref.**, **Titre**, **Nom auteur**, **Prénom auteur**, **Mots clé**, **Résumé**.
Pour trier la liste on peut utiliser les boutons sous la liste (**REF**, **TITRE**, **AUT. NOM**, etc.): le premier clic fait un tri croissant, le deuxième un tri décroissant (inverse).

Enfin, trois boutons se trouvent en bas de la fenêtre:
- Fiche du livre : pour consulter ou modifier des fiches des livres existants
- Nouveau livre : pour créer des nouvelles fiches
- Importer fichier de livres : pour créer des fiches à partir d'un fichier (fonctionnalité non développée encore)
- Sortir : pour revenir à la fenêtre principale

### Consulter ou modifier des fiches des livres
Le bouton **Fiche du livre** permet d'accéder à la fiche du livre sélectionné de la liste.
Depuis la fiche du livre nous pouvons cette fois modifier les informations (à différence de l'interface **bibou simple**).
Certains champs sont éditables par un champ de texte (REF, ISBN, TITRE, MOTS CLE, RESUME). D'autres champs sont éditables par une liste déroulante (AUTEUR, EDITEUR, ETAT).

- Les boutons **Précédent** et **Suivant** permettent de naviguer parmi les fiches de livres qui ont été éventuellement filtrées auparavant.
- Pour défaire les modifications réalisées il faut cliquer sur le bouton **Défaire les modifications**.
- Pour sauvegarder les modifications il faut cliquer sur le bouton **Mettre à jour** (ATTENTION! pas de marche arrière: les modifications seront introduites dans la base de données!).
- Pour revenir au catalogue il faut cliquer sur **Retour**

### Créer des nouvelles fiches de livres / auteurs / éditeurs
Le bouton **Nouveau livre** donne accès à un nouveau formulaire.
Les champs REF, ISBN, TITRE, MOTS CLE et RESUME sont directement renseignés en tapant sur les champs de texte respectifs.
Par contre, les auteurs et éditeurs doivent exister dans la base de données au préalable.
Pour cela, il faut cliquer sur les boutons **chercher auteur** et **chercher éditeur**.

En cliquant sur le bouton **chercher auteur** on accède au formulaire de recherche d'auteurs.
Pour filtrer cette liste nouos pouvons renseigner quelques lettres du prénom et/ou du nom de l'auteur dans les champs de texte correspondants.
Ensuite, on peut cliquer sur l'auteur recherché dans la liste et le sélectionner (bouton **SELECTIONNER**).
Le bouton **EFFACER FORMULAIRE** nous permet d'effacer les champs de texte (filtres).

Si la recherche est infructueuse, nous pourrons créer l'auteur.
Pour cela, nous allons cliquer sur le bouton **NOUVEL AUTEUR**, qui nous mène à un formulaire avec deux champs (NOM, PRENOM).

Nous pouvons aussi modifier un auteur existant en le séélectionnant dans la liste et ensuite en cliquant sur **MODIFIER AUTEUR**.

Le bouton **EXPORTER LISTE** permettrait d'exporter une liste d'auteurs (en appliquant des filtres NOM / PRENOM) sur un fichier **.xls**. Cette fonctionnalité n'est pas encore dééveloppée.

La modification et création d'éditeur se fait de la même manière que pour les auteurs.

## Gestion des élèves
Le bouton **GESTION DES ELEVES** mène au formulaire pour la création et modification des données d'élèves (NOM, PRENOM, NIVEAU).
Le comportement de ce formulaire est similaire à celui des auteurs, c'est à dire: d'abord nous avons la liste d'élèves. Nous pouvons filtrer cette liste pour avoir une liste plus restreinte en renseignant un ou plusieurs champs de texte (NOM, PRENOM, NIVEAU).
Ensuite, nous pouvons sélectionner un élève de la liste pour le modifier (bouton **MODIFIER ELEVE**).

Nous pouvons aussi créer un nouvel élève (bouton **NOUVEL ELEVE**), exporter la liste (filtrée ou pas) dans un fichier **.xls** (bouton **EXPORTER LISTE**) ou, inversement, importer un fichier **.xls** pour créer des nouveaux élèves ou modifier des élèves qui existent déjà.

Le format des fichiers **.xls** doit être le suivant pour la **création de nouveaux élèves**:


| PRENOM | NOM | NIVEAU |
| ------ | ------ | ------ |
| Albert | Camus | 1 |
| Victor | Hugo | 2 |

Le format des fichiers **.xls** doit être le suivant pour la **modification d'élèves existants**:


| ID | PRENOM | NOM | NIVEAU |
| ------ | ------ | ------ | ------ |
| 975 | Albert | Camus | 1 |
| 984 | Victor | Hugo | 2 |

Ce dernier est aussi le format des fichiers obtenus avec le bouton **EXPORTER LISTE**.

### :warning: ASTUCE :warning:

Pour la modification de niveau (nouvelle année scolaire), on peut:
1. Filtrer les élèves par niveau
1. Exporter la liste
1. Éditer le fichier **.xls** (copier/coller le nouveau niveau)
1. Importer la liste avec le nouveau niveau
1. Répéter pour chaque niveau en commançant du plus haut au plus bas (5, 4, 3, ...). Pour les élèves qui ont quitté l'école on peut attribuer un niveau inexistant (p.ex.: 99).

## Statistiques de la bibliothèque
Fonctionalité pas encore développée
