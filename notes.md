
###PySimpleGUI
####Comment passer d'une fenêtre principale à une fenêtre Enfant:
- Close() vs. make(): façon impropre de faire disparaître (main loop existe, mais la fenêtre non, on en crèe une autre)
- Hide() vs. Show(): la fenêtre disparait, mais quand elle reapparait toutes les valeurs sont remises a NULL
- Disappear() vs. Reappear(): la fenêtre disparait, mais quand elle reapparait elle conserve les valeurs introduites
###SQLite
####Utiliser les raccourcis:
- r = conIn.execute(sql).fetchall()
- r = conIn.executemany(sql).fetchall()
####Utiliser try catch: