![Bibou logo](./app/images/owl.png)
# Bibou, logiciel de gestion d'une bibliothèque (livres)

Bibou est un logiciel pour gérer facilement une bibliothèque.

Le public visé est, par exemple, une bibliothèque d'école gérée par des enfants.

Son **interface principale** permet de gérer les prêts d'ouvrages (livres).
Elle est conçue pour être accessible à des personnes sans connaissances
informatiques (p.ex.: gestion des prêts/retour de livres par des enfants).

Une **interface avancée** permet le gestion du catalogue et des utilisateurs.
Certaines notions d'informatique sont nécessaires (p.ex.: gestion de fichiers).

> :warning: Bibou est encore en développement, aucune version fonctionnelle n'est
disponible pour le moment.

## Principales caractéristiques
- simplicité d'utilisation
- légèreté (peu de fichiers, dépendances)

## Principales Fonctionnalités

### Interface simple: gestion des prêts
La **gestion de prêts** (prêt/retour/retard, etc.) est simple d'utilisation, adaptée à des personnes avec peu de connaissances informatiques.

Le manuel d'utilisation de l'interface simple se trouve [ici](./documentation/instructions_bibou_simple.md)

### Interface avancée: gestion de la bibliothèque
- **Gestion d'ouvrages**: on peut ajouter des nouveaux titres un par un, ou importer les données depuis un tableur (xls, csv).
- **Gestion utilisateurs**: on peut ajouter des nouveaux utilisateurs un par un, ou importer les données depuis un tableur (xls, csv).

Le manuel d'utilisation de l'interface avancée se trouve [ici](./documentation/instructions_bibou_advanced.md)

## Structure
- Langage de programmation: **Python** (multiplateforme, libre, code lisible)
- Base de données: **SQLite** (légèreté, multiplateforme)
- Fichiers de configuration: **settings.cfg** (fichier texte)

## Installation (testé sur raspbian)

1. Installation des paquets suivants : git, python3-tk (tkinter) et python3-pip (pip3)

```
# apt install git python3-tk python3-pip
```

2. Obtenir bibou à partir du dépôt git:

```
~ git clone https://framagit.org/eredondo/bibou.git bibou
```

3. Installer les dépendances python:

```
~ cd bibou/
~ pip3 install -r requirements.txt
```

4. Lancer l'installateur (création de fichiers *.desktop):

```
~ ./bibou_installer.py
```

5. copier les fichiers desktop dans le Bureau (par exemple):

```
~ cp *.desktop ~/Desktop/
```

6. Lancer l'interface simple et/ou avancée en double cliquant sur les icônes "Bibou" présents dans le Bureau

