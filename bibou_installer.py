#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bibou installer: make desktop files
Created on Sep 24 2020

@author: redondo
"""
import os

def check_modules():
    try:
        import tkinter
        print("OK: module 'tkinter' is installed")
    except ModuleNotFoundError:
        print("NOK: module 'tkinter' is not installed")
    try:
        import xlrd
        print("OK: module 'xlrd' is installed")
    except ModuleNotFoundError:
        print("NOK: module 'xlrd' is not installed")
    try:
        import xlwt
        print("OK: module 'xlwt' is installed")
    except ModuleNotFoundError:
        print("NOK: module 'xlwt' is not installed")
    try:
        import PySimpleGUI
        print("OK: module 'PySimpleGUI' is installed")
    except ModuleNotFoundError:
        print("NOK: module 'PySimpleGUI' is not installed")

def mk_bibou_desktop():
    print("Make bibou desktop file...")
    PWD = os.getcwd()
    BIBOU_APP = os.path.join(PWD,'app')
    if not os.path.isdir(BIBOU_APP):
        print("ERROR: app folder not found: " + BIBOU_APP)
        return -2
    BIBOU_PATH = os.path.join(BIBOU_APP,'bibou.py')
    if not os.path.isfile(BIBOU_PATH):
        print("ERROR: bibou.py not found")
        return -1
    BIBOU_ICON = os.path.join(BIBOU_APP,'images','owl.png')
    f = open("bibou.desktop", "w")
    f.write("[Desktop Entry]\n")
    f.write("Name=Bibou - gestion des prêts\n")
    f.write("Comment=Interface simple de bibou\n")
    f.write("Exec=" + BIBOU_PATH + "\n")
    f.write("Path=" + BIBOU_APP + os.sep + "\n")
    f.write("Terminal=false\n")
    f.write("Type=Application\n")
    f.write("Icon=" + BIBOU_ICON)
    f.close()
    os.chmod("bibou.desktop", 0o755)
    print("OK: bibou.desktop in current folder '" + os.getcwd() + "'")
    return 0

def mk_bibou_adv_desktop():
    print("Make bibou advanced desktop file...")
    PWD = os.getcwd()
    BIBOU_APP = os.path.join(PWD,'app')
    if not os.path.isdir(BIBOU_APP):
        print("ERROR: app folder not found: " + BIBOU_APP)
        return -2
    BIBOU_PATH = os.path.join(BIBOU_APP,'bibou_advanced.py')
    if not os.path.isfile(BIBOU_PATH):
        print("ERROR: bibou_advanced.py not found")
        return -1
    BIBOU_ICON = os.path.join(BIBOU_APP,'images','owl.png')
    f = open("bibou_adv.desktop", "w")
    f.write("[Desktop Entry]\n")
    f.write("Name=Bibou - interface avancée\n")
    f.write("Comment=Interface avancée de bibou\n")
    f.write("Exec=" + BIBOU_PATH + "\n")
    f.write("Path=" + BIBOU_APP + os.sep +"\n")
    f.write("Terminal=false\n")
    f.write("Type=Application\n")
    f.write("Icon=" + BIBOU_ICON)
    f.close()
    os.chmod("bibou_adv.desktop", 0o755)
    print("OK: bibou_adv.desktop in current folder '" + os.getcwd() + "'")
    return 0

if __name__ == '__main__':
    mk_bibou_desktop()
    mk_bibou_adv_desktop()
    check_modules()
